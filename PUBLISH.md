# Veröffentlichungsprozess

**Working Branch: `develop`**

Der Veröffentlichungsprozess ist mit Lerna.js teilweise automatisiert.

Vor der Veröffentlichung sollten folgende Schritte durchgeführt werden:

1. Überprüfen, ob alle Änderungen im develop vorhanden sind:

```bash
pnpm i && pnpm -r build
```

2. Falls noch uncommittete Änderungen vorhanden sind, diese committen und pushen.

**Release erstellen:**

```bash
lerna publish --conventional-commits --force-publish 
#Force Publish, damit alle Packages die gleiche Versionsnummer haben
#Conventional Commits damit eine ChangeLog generiert wird
```

Im Lerna Prozess wird dann abgefragt ob ein Patch, Minor oder Major Release erstellt werden soll.
Nach dem Release wird automatisch ein neuer Tag erstellt.
NPM fragt dann noch nach den Zugangsdaten für das NPM Repository. (2FA erforderlich)
