# KERN - UX-Standard für die deutsche Verwaltung

Mit KERN schaffen wir einen offenen UX Standard für die deutsche Verwaltung, der Umsetzende befähigt, barrierefreie digitale Verwaltungslösungen effizient zu entwickeln.
Die Länder Hamburg und Schleswig-Holstein bündeln ihre Kräfte, um KERN im Sinne des EfA-Gedankens zu initiieren. Nach dem Prinzip "Public Money - Public Code" veröffentlichen wir dafür ein qualitatives Open-Source-Design System und vervollständigen dies durch Handreichungen und Services rund um nutzer:innenzentrierte Digitalisierung. Die enthaltene technologieunabhängige Komponentenbibliothek baut auf [KoliBri] vom [ITZBund] auf, basiert wie dieses auf den Webstandards des W3C und ermöglicht eine visuelle Individualisierung durch Multi-Theming.

Die Bekanntmachung und Nutzung stellen wir durch die Zusammenarbeit mit einer länderübergreifenden Fach-Community (Bottom up) und Partnerschaften auf Entscheidungsebene (Top Down) von Anfang an sicher. KERN als Baukasten für digitale Verwaltungslösungen zahlt durch die Vereinheitlichung von Lösungen der "Marke Öffentliche Verwaltung” auf Kommunal- bis Bundesebene auf das Serviceerlebnis und Vertrauen der Bürger:innen ein.

## Changelog
[Die Changelog Datei ist auf OpenCode zu finden.](https://gitlab.opencode.de/kern-ux/pattern-library/-/blob/develop/CHANGELOG.md?ref_type=heads)

## Versionierung

KERN folgt den Prinzipien des semantischen Versionierung.

Aufbau einer Version:

- besteht in der Regel aus 3 Teilen (z.B. 1.0.2)
  - Major, hier die _1_
  - Minor, hier die _0_
  - Patch, hier die _2_
- für die Härtungsphase einer Version, kann man zusätzlich Labels verwenden (z.B. 1.0.3-rc.2)
  - Label, hier die _rc.2_

Folgende Hauptprinzipien kommen dabei zur Anwendung:

- **Patch**: Beinhaltet Änderungen die den aktuellen Funktionsumfang verbessern und in seiner Verwendung nicht ändern.
- **Minor**: Beinhaltet Änderungen die den Funktionsumfang erweitern und den bestehenden Funktionsumfang in seiner Verwendung nicht ändern.
- **Major**: Beinhaltet Änderungen die eine architektonische Neuausrichtung ermöglichen und den bestehenden Funktionsumfang in seiner Verwendung ändern dürfen.

Ausführliche Version der SemVer finden Sie hier: [https://semver.org]

## Kooperation

KERN ist ein Open-Source-Projekt, das von der Ländern Hamburg und Schleswig-Holstein initiiert wurde. Die Entwicklung erfolgt in enger Zusammenarbeit mit der Community.
Zur Community gehört unter anderem [KoliBri], auf dessen [Komponentenbibliothek] KERN aufbaut.

[ITZBund]: https://www.itzbund.de
[KoliBri]: https://public-ui.github.io
[Komponentenbibliothek]: https://github.com/public-ui/kolibri
[https://semver.org]: https://semver.org
