## User Story

Erstelle eine kurze Beschreibung des Bedarfs im Format einer User Story:\
\
**Als** \<Nutzende\> \
**möchte/benötige ich** \<ein Ziel\>, \
**damit/um** \<ein Grund\>.

**Beispiel:**

**Als** Bürger:in\
**möchte ich** online ein Datum für einen Termin beim Bürgeramt frei auswählen,\
**damit** ich einen Termin beim Amt meiner Wahl vereinbaren kann.

## Begründung

Erläutere anhand der drei folgenden Kriterien, warum diese Komponente zum KERN Design-System hinzugefügt werden sollte.

#### Alternativen

Warum kann der Anwendungsfall nicht mit einer bereits vorhandenen Komponente umgesetzt werden?

#### Skalierbarkeit

Welche Anhaltspunkte hast du dafür, dass die Komponente für verschiedene Anwendungsfälle benötigt wird?

#### **Usability**

Welche Erkenntnisse hast du dafür, dass es den Bedürfnissen der Nutzenden dieser Dienste entspricht?

## **Kontext (Optional)**

Füge Links, Bilder oder Dokumente zu Beispielen ein, falls vorhanden.

## Reifegrad

In welchem Stadium befindet sich die vorgeschlagene Komponente? Hast du einen **Entwurf, Design** oder schon im **Code** umgesetzte Komponente? 

## **Zeitliche Anforderung**

Zu welchem Zeitpunkt wird die beschriebene Komponente benötigt?

## **Beitragen**

Welche Expertise und/oder wieviel Zeit kannst du oder deine Organisation einbringen?

**Expertise**

z.B. UX Design, UI Design, Research

#### **Verfügbarkeit**

* [ ] nein, liegt bei KERN
* [ ] ja, ab KW X mit X Stunden.

Informiere dich vorab schon über unser Vorgehen.