/* eslint-disable */
/* tslint:disable */
/* auto-generated react proxies */
import { createReactComponent } from './react-component-lib';

import type { JSX } from '@kern-ux/components';



export const KernAccordion = /*@__PURE__*/createReactComponent<JSX.KernAccordion, HTMLKernAccordionElement>('kern-accordion');
export const KernAccordionGroup = /*@__PURE__*/createReactComponent<JSX.KernAccordionGroup, HTMLKernAccordionGroupElement>('kern-accordion-group');
export const KernAlert = /*@__PURE__*/createReactComponent<JSX.KernAlert, HTMLKernAlertElement>('kern-alert');
export const KernBadge = /*@__PURE__*/createReactComponent<JSX.KernBadge, HTMLKernBadgeElement>('kern-badge');
export const KernButton = /*@__PURE__*/createReactComponent<JSX.KernButton, HTMLKernButtonElement>('kern-button');
export const KernButtonGroup = /*@__PURE__*/createReactComponent<JSX.KernButtonGroup, HTMLKernButtonGroupElement>('kern-button-group');
export const KernDialog = /*@__PURE__*/createReactComponent<JSX.KernDialog, HTMLKernDialogElement>('kern-dialog');
export const KernDivider = /*@__PURE__*/createReactComponent<JSX.KernDivider, HTMLKernDividerElement>('kern-divider');
export const KernFieldset = /*@__PURE__*/createReactComponent<JSX.KernFieldset, HTMLKernFieldsetElement>('kern-fieldset');
export const KernForm = /*@__PURE__*/createReactComponent<JSX.KernForm, HTMLKernFormElement>('kern-form');
export const KernHeading = /*@__PURE__*/createReactComponent<JSX.KernHeading, HTMLKernHeadingElement>('kern-heading');
export const KernIcon = /*@__PURE__*/createReactComponent<JSX.KernIcon, HTMLKernIconElement>('kern-icon');
export const KernInputCheckbox = /*@__PURE__*/createReactComponent<JSX.KernInputCheckbox, HTMLKernInputCheckboxElement>('kern-input-checkbox');
export const KernInputDate = /*@__PURE__*/createReactComponent<JSX.KernInputDate, HTMLKernInputDateElement>('kern-input-date');
export const KernInputEmail = /*@__PURE__*/createReactComponent<JSX.KernInputEmail, HTMLKernInputEmailElement>('kern-input-email');
export const KernInputFile = /*@__PURE__*/createReactComponent<JSX.KernInputFile, HTMLKernInputFileElement>('kern-input-file');
export const KernInputNumber = /*@__PURE__*/createReactComponent<JSX.KernInputNumber, HTMLKernInputNumberElement>('kern-input-number');
export const KernInputPassword = /*@__PURE__*/createReactComponent<JSX.KernInputPassword, HTMLKernInputPasswordElement>('kern-input-password');
export const KernInputRadio = /*@__PURE__*/createReactComponent<JSX.KernInputRadio, HTMLKernInputRadioElement>('kern-input-radio');
export const KernInputText = /*@__PURE__*/createReactComponent<JSX.KernInputText, HTMLKernInputTextElement>('kern-input-text');
export const KernKopfzeile = /*@__PURE__*/createReactComponent<JSX.KernKopfzeile, HTMLKernKopfzeileElement>('kern-kopfzeile');
export const KernLink = /*@__PURE__*/createReactComponent<JSX.KernLink, HTMLKernLinkElement>('kern-link');
export const KernLinkButton = /*@__PURE__*/createReactComponent<JSX.KernLinkButton, HTMLKernLinkButtonElement>('kern-link-button');
export const KernLinkGroup = /*@__PURE__*/createReactComponent<JSX.KernLinkGroup, HTMLKernLinkGroupElement>('kern-link-group');
export const KernLoader = /*@__PURE__*/createReactComponent<JSX.KernLoader, HTMLKernLoaderElement>('kern-loader');
export const KernModal = /*@__PURE__*/createReactComponent<JSX.KernModal, HTMLKernModalElement>('kern-modal');
export const KernProgress = /*@__PURE__*/createReactComponent<JSX.KernProgress, HTMLKernProgressElement>('kern-progress');
export const KernSelect = /*@__PURE__*/createReactComponent<JSX.KernSelect, HTMLKernSelectElement>('kern-select');
export const KernTasklist = /*@__PURE__*/createReactComponent<JSX.KernTasklist, HTMLKernTasklistElement>('kern-tasklist');
export const KernTasklistGroup = /*@__PURE__*/createReactComponent<JSX.KernTasklistGroup, HTMLKernTasklistGroupElement>('kern-tasklist-group');
export const KernTasklistItem = /*@__PURE__*/createReactComponent<JSX.KernTasklistItem, HTMLKernTasklistItemElement>('kern-tasklist-item');
export const KernText = /*@__PURE__*/createReactComponent<JSX.KernText, HTMLKernTextElement>('kern-text');
export const KernTextarea = /*@__PURE__*/createReactComponent<JSX.KernTextarea, HTMLKernTextareaElement>('kern-textarea');
