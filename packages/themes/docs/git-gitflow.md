# GIT und GITFLOW

**Das Dokument befindet sich in einem frühen Entwicklungsstadium und diese Anleitung kann sich ändern.
Sie sind herzlich eingeladen, dieses Dokument zu verbessern und weiter zu entwickeln.**

## Vorwort

Bei der Entwicklung von Online Diensten(OD) ist es erforderlich git und gitflow einzusetzen.

Entwicklung eines Feature in OD wird in einem (gitflow) Feature-Branch entwickelt.

Um die Entwicklung zu Versionieren wird ein (gitflow)Release-Branch mit dem Inkrementellen Namen Bsp. [(0.2.0)](#Versionierung)(minor release) erstellt.

Bei Beendigung(Finish) des Release-Branch wird automatisch ein Tag mit dem selben Namen [(0.2.0)](#Versionierung) angelegt.

Sollten Hotfixes auf der Master-Branch angelegt werden, sind sie nach der selben Inkrementellen Nahmensgebung wie Feature-Branchen zu benennen. Bsp: [(0.2.1)](#Versionierung)(patch level).

Bei Git und Gitflow Fragen wenden Sie sich Bitte an:
[Darko Pervan](mailto:darko.pervan@dataport.de)

Im folgenden finden Sie eine Linksammlung sowie eine Übersicht über [Git](#Git-Cheatsheet) inf [Gitflow](#Gitflow-Cheatsheet) Befehle.

## Versionierung

```bash
0.2.1
│ │ └───────── Revisionsnummer (patch level)
│ └─────────── Nebenversionsnummer (minor release)
└───────────── Hauptversionsnummer (major release)
```

Wir benutzen [SemVer](http://semver.org/) für Versionierung.

## Git-Cheatsheet

### Links und Bücher

[Git Community Book](https://book.git-scm.com/)

[git - Der einfache Einstieg](https://rogerdudler.github.io/git-guide/index.de.html)

### git installieren

[Git für OSX](http://git-scm.com/download/mac)

[Git für Win](http://msysgit.github.io/)

[Git für Linux](http://book.git-scm.com/2_installing_git.html)

### Git Befehle (Beispiele)

> Git Überblick in einer grafischen Darstellung
>
>![image](images/cheat-sheet-graphic-v1.jpg "Git Überblick in einer grafischen Darstellung")

```bash
# git konfigurieren
git config --global user.email "email@mii.com"
git config --global user.name "My Name"

# neues repository erstellen
git init

# ein repository auschecken
git clone https://github.com/darkop/git-cheatsheet.git

# add & commit
git status                                    # status abfragen
git add <dateiname>
git add .
git commit -am "Commit-Nachricht"

# änderungen hochladen
git remote -v                                 # remote branch prüfen
git remote add origin <server>                # mit remote branch verbinden, wenn nicht geklont
git push origin master                        # entfernte repo updaten

# branching
git checkout -b feature_x                     # branch anlegen und wechseln
git checkout -b feature_x feature_y           # branch basiert auf
git checkout master                           # zu master wechseln
git push -u origin feature_x                  # branch remote anlegen
git push origin feature_x                     # remote branch updaten
git branch -d feature_x                       # branch löschen
git push origin :feature_x                    # remote branch löschen

# update & merge
git pull                                      # fatch and merge
git merge <branch>                            # branches zusammenführen
git add <dateiname>                           # nach dem konflikt
git diff <quell_branch> <ziel_branch>         # vor dem merge Differenzen anschauen

# tagging
git tag 1.0.0 1b2e1d63ff                      # Releasestags anlegen
git log                                       # die Liste der Commit-IDs

# änderungen rückgängig machen
git checkout -- <filename>                    # Datei zum letzten Stand im HEAD zurücksetzen
git checkout -- .                             # alle änderungen verwerfen
# lokalen Änderungen komplett entfernen
git fetch origin
git reset --hard origin/master

# nützliche tricks

git rm --cached /path/to/file                 # entferne Datei aus dem git ohne es zu löschen, in .gitignore eintragen
git log --graph --oneline --all               # Git tree in terminal
gitk                                          # Eingebaute git-GUI
git config color.ui true                      # Farbige Konsolenausgabe:
git config format.pretty oneline              # Einzeilige Commit Logausgabe
git add -i                                    # Interaktives Hinzufügen von Änderungen
git merge v1.0 --no-commit --no-ff            # merge ohne auto commit
git clean -f								  # alle hinzigefügten Dateien vor dem Commit entfernen

# Tag nachträglich umbenennen local und remote
git tag neu alt                                   # tag umbenennen Bsp. "git tag 0.1.1 hotfix_branch"
git tag -d alt                                    # alten tag löschen
git push origin :refs/tags/old                    # alten tag remote löschen
git push --tags                                   # neuen tag publizieren
# informiere deine Kollegen darüber und bitte Sie die Tags, mit folgenden kommando, bei sich zu aktuallisieren
git pull --prune --tags                           # aktuelle tags holen
git rm -r --cached .							  # lösche hinzigefügte dateien die nachtreglich auf ignore gesetzt sind 

# Cleaning up old remote git branches
git remote prune origin
# oder
git fetch origin --prune

```

### .gitignore

Link zu dem [.gitignore](https://github.com/github/gitignore) Dateien für unterschiedliche Projekte.

### .gitconfig

Beispiel einer [gitconfig](https://gist.github.com/pksunkara/988716) Datei

## Gitflow

### Gitflow-Cheatsheet

> Git branching model
>
>![image](images/git-model@2x.png "Git branching model")

### Gitflow Befehle (Beispiele)

```bash
git flow init -d                                  # initiert gitflow im Projekt und -d benutzt Standard branch benennung

# feature

# Nur nn featuer brach wird entwickelt.
# vermeidet die Arbeit auf der develop Branch.

git flow feature start first_feature              # eine feature branch anlegen
git flow feature publish first_feature            # feature remote veröffentlichen
git flow feature pull origin first_feature        # holen letzte updates von remote origin
git flow feature track first_feature              # feature von remote holen
git add .
git commit -am "#12345 add my first feature"
git flow feature finish first_feature             # featuer wird abgeschlossen und nach develop gemergt, anschliersend (nur lokal) gelöscht
git push origin develop

# release

# release brach solle nur kurz (1-2 Tage) leben.
# release muss numerisch benannt werden (0.2.0), wird als tag angelegt.

git flow release start 0.2.0                      # release branch anlegen
git flow release publish 0.2.0                    # release branch veröffentlichen
git flow release track 0.2.0                      # release von remote holen
git flow release finish 0.2.0                     # release nach master und develop mergen
git push origin develop
git checout master
git push origin master
git push --tags                                   # tag (0.2.0) veröffentlichen

# horfix

# hotfix muss numerisch benannt werden (0.2.1), wird als tag angelegt.

git flow hotfix start 0.2.1
git flow hotfix finish 0.2.1
```

> Überblich über Gitflow Befehle 
>
>![image](images/git-flow-commands.png "Gitflow Befehle")
>
>Basp.: `$ git flow featuer start new_featuer`

### Links

[A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)

[git-flow Spickzettel](https://danielkummer.github.io/git-flow-cheatsheet/index.de_DE.html)

[Gitflow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

[Git Flow Workflow 2](https://leanpub.com/git-flow/read)

[Git-Workflows - Der Gitflow-Workflow](https://infos.seibert-media.net/display/Productivity/Git-Workflows+-+Der+Gitflow-Workflow)

[Using git-flow to automate your git branching workflow](https://jeffkreeftmeijer.com/git-flow/)

[Guide to using GitFlow](https://blogs.endjin.com/2013/04/a-step-by-step-guide-to-using-gitflow-with-teamcity-part-3-gitflow-commands/)

### git-flow installieren

[Mac OS X](https://github.com/nvie/gitflow/wiki/Mac-OS-X)

[Linux](https://github.com/nvie/gitflow/wiki/Linux)

[Windows](https://github.com/nvie/gitflow/wiki/Windows)

### Gitflow und Git Befehle nebeneinander

gitflow | git
--------|-----
`git flow init` | `git init`
&nbsp; | `git commit --allow-empty -m "Initial commit"`
&nbsp; | `git checkout -b develop master`

### Mit entfernter repo verbinden

gitflow | git
--------|-----
_N/A_ | `git remote add origin <server>`

### Features

#### feature branch erstellen

gitflow | git
--------|-----
`git flow feature start MYFEATURE` | `git checkout -b feature/MYFEATURE develop`

#### feature branch hochladen

gitflow | git
--------|-----
`git flow feature publish MYFEATURE` | `git checkout feature/MYFEATURE`
&nbsp; | `git push origin feature/MYFEATURE`

#### lokale feature branch aktualisieren

gitflow | git
--------|-----
`git flow feature pull origin MYFEATURE` | `git checkout feature/MYFEATURE`
&nbsp; | `git pull --rebase origin feature/MYFEATURE`

#### feature branch abschließen

gitflow | git
--------|-----
`git flow feature finish MYFEATURE` | `git checkout develop`
&nbsp; | `git merge --no-ff feature/MYFEATURE`
&nbsp; | `git branch -d feature/MYFEATURE`

#### Push und merged feature branch

gitflow | git
--------|-----
_N/A_ | `git push origin develop`
&nbsp; | `git push origin :feature/MYFEATURE` _(if pushed)_

### Releases

#### release branch erstellen

gitflow | git
--------|-----
`git flow release start 1.2.0` | `git checkout -b release/1.2.0 develop`

#### release branch hochladen

gitflow | git
--------|-----
`git flow release publish 1.2.0` | `git checkout release/1.2.0`
&nbsp; | `git push origin release/1.2.0`

#### lokale release branch aktualisieren

gitflow | git
--------|-----
_N/A_ | `git checkout release/1.2.0`
&nbsp; | `git pull --rebase origin release/1.2.0`

#### release branch abschließen

gitflow | git
--------|-----
`git flow release finish 1.2.0` | `git checkout master`
&nbsp; | `git merge --no-ff release/1.2.0`
&nbsp; | `git tag -a 1.2.0`
&nbsp; | `git checkout develop`
&nbsp; | `git merge --no-ff release/1.2.0`
&nbsp; | `git branch -d release/1.2.0`

#### Push merged feature branch

gitflow | git
--------|-----
_N/A_ | `git push origin master`
&nbsp; | `git push origin develop`
&nbsp; | `git push origin --tags`
&nbsp; | `git push origin :release/1.2.0` _(if pushed)_

### Hotfixes

#### hotfix branch erstellen

gitflow | git
--------|-----
`git flow hotfix start 1.2.1 [commit]` | `git checkout -b hotfix/1.2.1 [commit]`

#### hotfix branch abschliessen

gitflow | git
--------|-----
`git flow hotfix finish 1.2.1` | `git checkout master`
&nbsp; | `git merge --no-ff hotfix/1.2.1`
&nbsp; | `git tag -a 1.2.1`
&nbsp; | `git checkout develop`
&nbsp; | `git merge --no-ff hotfix/1.2.1`
&nbsp; | `git branch -d hotfix/1.2.1`

#### Push merged hotfix branch

gitflow | git
--------|-----
_N/A_ | `git push origin master`
&nbsp; | `git push origin develop`
&nbsp; | `git push origin --tags`
&nbsp; | `git push origin :hotfix/1.2.1` _(if pushed)_