import { readFile, writeFile, readdir, rename } from "fs/promises";
import { join, parse } from "path";
import * as sass from "sass";
import { format } from "prettier";
import parserTypeScript from "prettier/esm/parser-typescript.mjs";
import { PrettyConsole } from './prettyConsole.js';
const prettyConsole = new PrettyConsole();

export async function main(themeName = "core") {
	prettyConsole.info(`Creating ${themeName} Theme...`);
	let directory = `./src/themes/${themeName}`;
	let coreDirectory = `./src/themes/core`; 
	let styleObject = {};
	let variablesObject = {};
	let useStatements = [];

	// Const to extract variables from scss files and store them in a object.
	const extractVars = (data, overwrite = false) => {
		let variablePattern = /\$[\w-]+:\s*[^;]+;|@use\s[^;]+;/g;
		let matches = data.match(variablePattern);

		if (matches) {
			matches.forEach(match => {
				if (match.includes("@use")) {
					useStatements.push(match);
				} else {
					let [key, value] = match.split(":");
					if (overwrite || !variablesObject.hasOwnProperty(key)) {
						variablesObject[key.trim()] = value.trim();
					}
				}
			});
		}
	};

		let coreData = await readFile(join(coreDirectory, "/scss/_variables.scss"), 'utf8');
		extractVars(coreData);
		let themeData = await readFile(join(directory, "/scss/_variables.scss"), 'utf8');
		extractVars(themeData, true);

		// TODO: Make this Prettie
		const variableData = useStatements.join('') + JSON.stringify(variablesObject).replace(/[{}"]/g, '').replaceAll(';,', ';');
		await writeFile(join(coreDirectory, "/scss/_test.scss"), variableData);
		console.log('writeFile');
	try {
		async function prepareVariableFile() {
			await rename(join(coreDirectory, "/scss/_variables.scss"), join(coreDirectory, "/scss/_temp.scss"));
			await rename(join(coreDirectory, "/scss/_test.scss"), join(coreDirectory, "/scss/_variables.scss"));

			prettyConsole.info(`Preparing variables file...`);
		}

		async function cleanVariableFile() {
			prettyConsole.info(`Cleaning up...`);
			// fs.unlink(join(coreDirectory, "/scss/_variables.scss")); // Delete old temp variables file
			await rename(join(coreDirectory, "/scss/_temp.scss"), join(coreDirectory, "/scss/_variables.scss")); // Rename old variables file back to original
			prettyConsole.info(`Cleaning finished`);
		}

		console.log("themeName", themeName);
		if (themeName !== "core") {
			await prepareVariableFile();
		}

		const files = await readdir(`${coreDirectory}/scss`);

		await Promise.all(
			files
				.filter((file) => !file.startsWith("_"))
				.map((file) => {
					return sass.compileAsync(join(coreDirectory, `/scss/${file}`), { style: "compressed" }).then((result) => {
						let fileKey = parse(file).name === "global" ? `${parse(file).name.toUpperCase()}` : `KOL-${parse(file).name.toUpperCase()}`;
						styleObject[fileKey] = result.css.toString().replace(/\n/g, "");
					});
				})
		);

		// console.log(`StyleObject`, styleObject);
		prettyConsole.success(`Successfully created ${themeName} Theme!`)
		if (themeName !== "core") {
			await cleanVariableFile();
		}

		let code = format(
			`
    import { KoliBri } from '@public-ui/schema';
    export const ${themeName.toString().toUpperCase()} = KoliBri.createTheme("${themeName}", ${JSON.stringify(styleObject)});
    `,
		{ parser: "typescript", plugins: [parserTypeScript] }
		);

		await writeFile(`src/themes/${themeName}/index.ts`, await code);
	} catch (error) {
		prettyConsole.error("Error occurred while compiling Sass files!", error);
		console.log(error)
	}
}
