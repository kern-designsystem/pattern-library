const source = './src/themes/';
import {readdir} from 'fs/promises';
import {main as sassCompiler} from './sassCompiler.js';

const getDirectories = async source => (await readdir(source, {withFileTypes: true})).filter(d => d.isDirectory()).map(d => d.name);

// finish and then run next sasscompiler
(async () => {
	const directories = await getDirectories(source);
	await directories.reduce(async (previousPromise, element) => {
		await previousPromise;
		return sassCompiler(element);
	}, Promise.resolve());
})();

