import { KoliBri } from '@public-ui/schema';
import globalCss from './scss/global.scss'; // Theme spezifisch
import accordionCss from '../core/scss/accordion.scss';
import alertCss from '../core/scss/alert.scss';
import badgeCss from '../core/scss/badge.scss';
import buttonCss from '../core/scss/button.scss';
import buttonGroupCss from '../core/scss/button-group.scss';
import headingCss from '../core/scss/heading.scss';
import iconCss from '../core/scss/icon.scss';
import inputCheckboxCss from '../core/scss/input-checkbox.scss';
import inputDateCss from '../core/scss/input-date.scss';
import inputEmailCss from '../core/scss/input-email.scss';
import inputFileCss from '../core/scss/input-file.scss';
import inputNumberCss from '../core/scss/input-number.scss';
import inputPasswordCss from '../core/scss/input-password.scss';
import inputRadioCss from '../core/scss/input-radio.scss';
import inputTextCss from '../core/scss/input-text.scss';
import inputTextareaCss from '../core/scss/input-textarea.scss';
import linkCss from '../core/scss/link.scss';
import linkButtonCss from '../core/scss/link-button.scss';
import modalCss from '../core/scss/modal.scss';
import progressCss from '../core/scss/progress.scss';
import selectCss from '../core/scss/select.scss';
import spinCss from '../core/scss/spin.scss';

export const EFA = KoliBri.createTheme('efa', {
	GLOBAL: globalCss,
	'KOL-ACCORDION': accordionCss,
	'KOL-ALERT': alertCss,
	'KOL-BADGE': badgeCss,
	'KOL-BUTTON': buttonCss,
	'KOL-BUTTON-GROUP': buttonGroupCss,
	'KOL-HEADING': headingCss,
	'KOL-ICON': iconCss,
	'KOL-INPUT-CHECKBOX': inputCheckboxCss,
	'KOL-INPUT-DATE': inputDateCss,
	'KOL-INPUT-EMAIL': inputEmailCss,
	'KOL-INPUT-FILE': inputFileCss,
	'KOL-INPUT-NUMBER': inputNumberCss,
	'KOL-INPUT-PASSWORD': inputPasswordCss,
	'KOL-INPUT-RADIO': inputRadioCss,
	'KOL-INPUT-TEXT': inputTextCss,
	'KOL-TEXTAREA': inputTextareaCss,
	'KOL-LINK': linkCss,
	'KOL-LINK-BUTTON': linkButtonCss,
	'KOL-MODAL': modalCss,
	'KOL-PROGRESS': progressCss,
	'KOL-SELECT': selectCss,
	'KOL-SPIN': spinCss,
});
