import { KoliBri } from '@public-ui/schema';
import { Kern } from '@kern-ux/schema';
import globalCss from './scss/index.scss';
import accordionCss from './scss/accordion.scss';
import alertCss from './scss/alert.scss';
import badgeCss from './scss/badge.scss';
import buttonCss from './scss/button.scss';
import buttonGroupCss from './scss/button-group.scss';
import headingCss from './scss/heading.scss';
import iconCss from './scss/icon.scss';
import inputCheckboxCss from './scss/input-checkbox.scss';
import inputDateCss from './scss/input-date.scss';
import inputEmailCss from './scss/input-email.scss';
import inputFileCss from './scss/input-file.scss';
import inputNumberCss from './scss/input-number.scss';
import inputPasswordCss from './scss/input-password.scss';
import inputRadioCss from './scss/input-radio.scss';
import inputTextCss from './scss/input-text.scss';
import inputTextareaCss from './scss/input-textarea.scss';
import linkCss from './scss/link.scss';
import linkButtonCss from './scss/link-button.scss';
import modalCss from './scss/modal.scss';
import progressCss from './scss/progress.scss';
import selectCss from './scss/select.scss';
import spinCss from './scss/spin.scss';

export const CORE = KoliBri.createTheme('core', {
	GLOBAL: globalCss,
	'KOL-ACCORDION': accordionCss,
	'KOL-ALERT': alertCss,
	'KOL-BADGE': badgeCss,
	'KOL-BUTTON': buttonCss,
	'KOL-BUTTON-GROUP': buttonGroupCss,
	'KOL-HEADING': headingCss,
	'KOL-ICON': iconCss,
	'KOL-INPUT-CHECKBOX': inputCheckboxCss,
	'KOL-INPUT-DATE': inputDateCss,
	'KOL-INPUT-EMAIL': inputEmailCss,
	'KOL-INPUT-FILE': inputFileCss,
	'KOL-INPUT-NUMBER': inputNumberCss,
	'KOL-INPUT-PASSWORD': inputPasswordCss,
	'KOL-INPUT-RADIO': inputRadioCss,
	'KOL-INPUT-TEXT': inputTextCss,
	'KOL-TEXTAREA': inputTextareaCss,
	'KOL-LINK': linkCss,
	'KOL-LINK-BUTTON': linkButtonCss,
	'KOL-MODAL': modalCss,
	'KOL-PROGRESS': progressCss,
	'KOL-SELECT': selectCss,
	'KOL-SPIN': spinCss,
});

export const KERNCORE = Kern.createTheme('core', {
	GLOBAL: globalCss,
	'KERN-KOPFZEILE': accordionCss,
});
