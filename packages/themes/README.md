# KERN Themes Repo ![Kern Logo](kern-logo.png)

Dieses Repository beinhaltet alle Themes, die von KERN angeboten werden.

## Ordnerstruktur

Unter `src/themes/` befinden sich in den jeweiligen Unterordnern die Themes der verschiedenen Mandanten.
Darin befinden sich die Styles im `/scss` Ordner. Unter `/dist` finden sich die kompilierten CSS Dateien.

## Build Prozess

- SCSS Dateien werden zu CSS Dateien kompiliert.
- CSS Dateien werden ausgelesen und ein Objekt gepackt.
- `index.ts` wird mit dem Style Objekt befüllt.

## Build Scripts

Mit folgenden Scripts können die Themes kompiliert und gebaut werden.

### Einzelnes Theme kompilieren (aus mehreren SCSS eine index.ts erstellen)

```bash
npm run compile-theme -- <THEME>
npm run compile-theme -- default
npm run compile-theme -- hh
```

### Alle Themes zu kompilieren

```bash
npm run compile-all
```

### Package erzeugen

```bash
npm run compile-all && npm run build
```

## Anwendung lokal starten

Pakete installieren

```bach
pnpm install
```

Anwendung starten

```bach
npm run vite
```
