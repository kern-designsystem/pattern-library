import { KernButtonGroup, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

const PROPS = {
	_label: 'Primary',
	_variant: 'primary',
};

class ButtonGroup extends React.Component {
	render() {
		return (
			<div className="component-section">
				<header className="section-header">
					<KernHeading _level="2" _label="Button"></KernHeading>
					<KernBadge _label="WIP" _status="warning" _icon="material-symbols-rounded filled small badge_warning"></KernBadge>
				</header>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Default"></KernHeading>
					<div className="component-row">
						{/* <KernButtonGroup>
                            <KernButton _label="Speichern" _variant="primary"></KernButton>
                            <KernButton _label="Speichern & Schließen" _variant="normal"></KernButton>
                            <KernButton _label="Abbrechen" _variant="secondary"></KernButton>
                            <KernButton _label="Löschen" _variant="danger"></KernButton>
                            <KernButton _label="Ghost" _variant="ghost"><KernButton>
                            <KernButton _label="Deaktiviert" _disabled></KernButton>
                        </KernButtonGroup> */}
					</div>
				</div>
			</div>
		);
	}
}
export default ButtonGroup;
