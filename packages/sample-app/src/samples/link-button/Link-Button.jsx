import { KernLinkButton, KernHeading, KernBadge, KernAlert } from '@kern-ux/react';
import React from 'react';

const PROPS = {
	_label: 'Primary',
	_variant: 'primary',
	_href: '#',
};

class LinkButton extends React.Component {
	render() {
		return (
			<div className="component-section" id="link-button">
				<header className="section-header">
					<KernHeading _level="2" _label="Link Button"></KernHeading>
					<div className="header-icons">
						<div className="component-row">
							<KernBadge _label="Style" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="React Adapter" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="Figma: v1.5.3" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
						</div>
					</div>
				</header>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Default"></KernHeading>
					<div className="component-row">
						<KernLinkButton {...PROPS}></KernLinkButton>
						<KernLinkButton _href='#' _label="Secondary" _variant="secondary"></KernLinkButton>
						<KernLinkButton _href='#' _label="Tertiary" _variant="tertiary"></KernLinkButton>
					</div>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="With Icon"></KernHeading>
					<div className="component-row">
						<KernLinkButton {...PROPS} _icons="{ 'left': { 'icon': 'material-symbols-outlined arrow_forward' } }"></KernLinkButton>
						<KernLinkButton _href='#' _label="Secondary" _variant="secondary" _icons="{ 'left': { 'icon': 'material-symbols-outlined arrow_back' } }"></KernLinkButton>
						<KernLinkButton _href='#' _label="Tertiary" _variant="tertiary" _icons="{ 'left': { 'icon': 'material-symbols-outlined arrow_forward' } }"></KernLinkButton>
					</div>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Icon Only"></KernHeading>
					<div className="component-row">
						<KernLinkButton _hide-label="true" {...PROPS} _icons="{ 'left': { 'icon': 'material-symbols-outlined arrow_forward' } }"></KernLinkButton>
						<KernLinkButton
							_href='#'
							_hide-label="true"
							_label="Secondary"
							_variant="secondary"
							_icons="{ 'left': { 'icon': 'material-symbols-outlined arrow_back' } }"
						></KernLinkButton>
						<KernLinkButton
							_href='#'
							_hide-label="true"
							_label="Tertiary"
							_variant="tertiary"
							_icons="{ 'left': { 'icon': 'material-symbols-outlined arrow_forward' } }"
						></KernLinkButton>
					</div>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Disabled"></KernHeading>
					<div className="component-row">
						<KernLinkButton {...PROPS} _disabled></KernLinkButton>
						<KernLinkButton _href='#' _label="Secondary" _variant="secondary" _disabled></KernLinkButton>
						<KernLinkButton _href='#' _label="Tertiary" _variant="tertiary" _disabled></KernLinkButton>
					</div>
				</div>
			</div>
		);
	}
}
export default LinkButton;
