import { KernText, KernHeading, KernBadge, KernTasklist, KernTasklistGroup, KernTasklistItem } from '@kern-ux/react';
import React from 'react';

class Tasklist extends React.Component {
	render() {
		return (
			<div className="component-section" id="tasklist">
				<header className="section-header">
					<KernHeading _level="2" _label="Tasklist"></KernHeading>
					<div className="header-icons">
						<KernBadge _label="Style" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="React Adapter" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Figma: v1.5.1" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
					</div>
				</header>

				<div className="component-col">
				<KernTasklist _id="tasklist_1" _numbered="true">
					<KernTasklistGroup _label="Aufgabengruppe 1">
						<KernTasklistItem _label="Aufgabe xy erledigen" _badge="complete" _is-editable="true" _href="/test"></KernTasklistItem>
						<KernTasklistItem _label="Aufgabe xy erledigen" _badge="incomplete" _is-editable="true"></KernTasklistItem>
						<KernTasklistItem _label="Aufgabe xy erledigen" _badge="error" _is-editable="true"></KernTasklistItem>
					</KernTasklistGroup>
					<KernTasklistGroup _label="Aufgabengruppe 2">
						<KernTasklistItem _label="Aufgabe xy erledigen" _badge="complete" _is-editable="true"></KernTasklistItem>
						<KernTasklistItem _label="Aufgabe xy erledigen" _badge="incomplete" _is-editable="true"></KernTasklistItem>
						<KernTasklistItem _label="Aufgabe xy erledigen" _badge="blocked" _is-editable="false"></KernTasklistItem>
						<KernTasklistItem _label="Aufgabe xy erledigen" _badge="empty" _is-editable="false"></KernTasklistItem>
						<KernTasklistItem _label="Aufgabe xy erledigen" _badge="" _is-editable="false"></KernTasklistItem>
					</KernTasklistGroup>
				</KernTasklist>
				</div>

			</div>
		);
	}
}
export default Tasklist;
