import { KernInputEmail, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class InputEmail extends React.Component {
	render() {
		return (
			<div className="component-section" id="input-email">
				<header className="section-header">
					<KernHeading _level="2" _label="Input Email"></KernHeading>
					<div className="header-icons">
						<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
					</div>
				</header>

				<div className="component-col">
					<KernInputEmail _label="Input Email"></KernInputEmail>
					<KernInputEmail
						_label="Input Email"
						_hint="Hint Text"
						_required
						_error="Ich bin die Fehlermeldung!"
					></KernInputEmail>
					<KernInputEmail
						_label="Input Email"
						_required
						_error="Ich bin die Fehlermeldung!"
					></KernInputEmail>
					<KernInputEmail _label="Input Email" _disabled></KernInputEmail>
					<KernInputEmail
						_label="Input Email"
						_disabled
						_hint="Hint Text"
					></KernInputEmail>
					<KernInputEmail _label="Input Email" _readOnly></KernInputEmail>
				</div>
			</div>
		);
	}
}
export default InputEmail;
