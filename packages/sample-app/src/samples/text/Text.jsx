import { KernText, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class Text extends React.Component {
	render() {
		return (
			<div className="component-section" id="text">
				<header className="section-header">
					<KernHeading _level="2" _label="Text"></KernHeading>
					<div className="header-icons">
						<KernBadge _label="Global Style" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="React Adapter" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
					</div>
				</header>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Default Text"></KernHeading>
					<div className="component-row">
						<KernText>
							Das ist ein Default Text. Das ist ein Default Text. Das ist ein Default Text. Das ist ein Default Text. Das ist ein Default Text. Das ist ein
							Default Text. Das ist ein Default Text.{' '}
						</KernText>
						<KernText _variant="default">
							Das ist ein Default Text. Das ist ein Default Text. Das ist ein Default Text. Das ist ein Default Text. Das ist ein Default Text. Das ist ein
							Default Text. Das ist ein Default Text.{' '}
						</KernText>
						<KernText _variant="default" _weight="bold">
							Das ist ein Default Text. Das ist ein Default Text. Das ist ein Default Text. Das ist ein Default Text. Das ist ein Default Text. Das ist ein
							Default Text. Das ist ein Default Text.{' '}
						</KernText>
					</div>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Large Text"></KernHeading>
					<div className="component-row">
						<KernText _variant="large">
							Das ist ein Large Text. Das ist ein Large Text. Das ist ein Large Text. Das ist ein Large Text. Das ist ein Large Text. Das ist ein Large Text.
							Das ist ein Large Text. Das ist ein Large Text.
						</KernText>
						<KernText _variant="large" _weight="bold">
							Das ist ein Large Text. Das ist ein Large Text. Das ist ein Large Text. Das ist ein Large Text. Das ist ein Large Text. Das ist ein Large Text.
							Das ist ein Large Text. Das ist ein Large Text.
						</KernText>
					</div>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Small Text"></KernHeading>
					<div className="component-row">
						<KernText _variant="small">
							Das ist ein Small Text. Das ist ein Small Text. Das ist ein Small Text. Das ist ein Small Text. Das ist ein Small Text. Das ist ein Small Text.
							Das ist ein Small Text. Das ist ein Small Text.
						</KernText>
						<KernText _variant="small" _weight="bold">
							Das ist ein Small Text. Das ist ein Small Text. Das ist ein Small Text. Das ist ein Small Text. Das ist ein Small Text. Das ist ein Small Text.
							Das ist ein Small Text. Das ist ein Small Text.
						</KernText>
					</div>
				</div>
			</div>
		);
	}
}
export default Text;
