import { KernDialog, KernButton, KernText, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class Dialog extends React.Component {
	render() {
		return (
			<div className="component-section" id="dialog">
				<header className="section-header">
					<KernHeading _level="2" _label="Dialog"></KernHeading>
					<div className="header-icons">
						<div className="component-row">
							<KernBadge _label="Style" _status="warning" _icon="material-symbols-outlined filled small badge_warning"></KernBadge>
							<KernBadge _label="React Adapter" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="Figma: v1.5.1" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
						</div>
					</div>
				</header>

				<div className="component-col">
					<KernDialog _id="modal-1" _label="Dialog Headline 1">
						<KernText _variant="default">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec purus nec nunc tincidunt.
						</KernText>
					</KernDialog>

					<KernDialog _id="modal-2" _label="Dialog Headline 2" _hasCloser>
						<KernText _variant="default">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec purus nec nunc tincidunt.
						</KernText>
					</KernDialog>

					<KernButton
						_customClass="block"
						_label="Modal öffnen mit Close und Backdrop"
						_variant="tertiary"
						_icons="{ 'left': { 'icon': 'material-symbols-outlined search' } }"
						_on={{ onClick: () => document.querySelector('#modal-1').showModal() }}
					></KernButton>

					<KernButton
						_customClass="block"
						_label="Modal öffnen"
						_variant="tertiary"
						_icons="{ 'left': { 'icon': 'material-symbols-outlined search' } }"
						_on={{ onClick: () => document.querySelector('#modal-2').show() }}
					></KernButton>
				</div>
			</div>
		);
	};
};
export default Dialog;
