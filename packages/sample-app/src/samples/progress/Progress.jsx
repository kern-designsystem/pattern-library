import { KernProgress, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class Progress extends React.Component {
	render() {
		return (
			<div className="component-section" id="progress">
				<header className="section-header">
					<KernHeading _level="2" _label="Progress"></KernHeading>
					<div className="header-icons">
						<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						{/* <KernBadge _label="WIP" _status="warning" _icons="material-symbols-rounded filled small badge_warning"></KernBadge> */}
						<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
					</div>
				</header>

				<div className="component-col">
				<KernProgress _variant="bar" _label="Schritt 3 von 5" _max="5" _value="3" _unit=""></KernProgress>

				</div>
			</div>
		);
	}
}
export default Progress;
