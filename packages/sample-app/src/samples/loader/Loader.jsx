import { KernLoader, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class Loader extends React.Component {
	render() {
		return (
			<div className="component-section" id="loader">
				<header className="section-header">
					<KernHeading _level="2" _label="Loader"></KernHeading>
					<div className="header-icons">
						<KernBadge _label="Style" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="React Adapter" _status="success" _icon="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
					</div>
				</header>

				<div className="component-col">
					<KernLoader _show="true"></KernLoader>
				</div>
			</div>
		);
	}
}
export default Loader;
