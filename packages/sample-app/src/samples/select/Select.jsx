import { KernSelect, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class Select extends React.Component {
	render() {
		return (
			<div className="component-section" id="select">
				<header className="section-header">
					<KernHeading _level="2" _label="Select"></KernHeading>
					<div className="header-icons">
						<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Figma: v1.5.1" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
					</div>
				</header>

				<div className="component-col">
					<KernSelect 
						_label="Required" 
						_options="[{'label':'Herr','value':0},{'label':'Frau','value':1},{'label':'Firma','value':2}]" 
						_value="[1]"
						_icons="{'right': { 'icon': 'material-symbols-outlined filled expand_more'}}"
						_required
						>
					</KernSelect>
					<KernSelect 
						_label="Label" 
						_options="[{'label':'Herr','value':0},{'label':'Frau','value':1},{'label':'Firma','value':2}]" 
						_value="[1]"
						_icons="{'right': { 'icon': 'material-symbols-outlined filled expand_more'}}"
						>
					</KernSelect>
					<KernSelect 
						_label="Label" 
						_options="[{'label':'Herr','value':0},{'label':'Frau','value':1},{'label':'Firma','value':2}]" 
						_value="[1]"
						_icons="{'right': { 'icon': 'material-symbols-outlined filled expand_more'}}"
						_disabled
						>
					</KernSelect>
					<KernSelect 
						_label="Mit Hint" 
						_options="[{'label':'Herr','value':0},{'label':'Frau','value':1},{'label':'Firma','value':2}]" 
						_value="[1]"
						_icons="{'right': { 'icon': 'material-symbols-outlined filled expand_more'}}"
						_hint="Hint Text"
						>
					</KernSelect>
					<KernSelect 
						_label="Mit Hint" 
						_required
						_error="Fehler Text"
						_options="[{'label':'Herr','value':0},{'label':'Frau','value':1},{'label':'Firma','value':2}]" 
						_value="[1]"
						_icons="{'right': { 'icon': 'material-symbols-outlined filled expand_more'}}"
						_hint="Hint Text"
						>
					</KernSelect>
				</div>
			</div>
		);
	}
}
export default Select;
