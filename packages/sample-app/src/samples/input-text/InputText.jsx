import { KernInputText, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class InputText extends React.Component {
	render() {
		return (
			<div className="component-section" id="input-text">
				<header className="section-header">
					<KernHeading _level="2" _label="Input Text"></KernHeading>
					<div className="header-icons">
						<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
						<KernBadge _label="Figma: v0.1.0" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
					</div>
				</header>

				<div className="component-col">
					<KernInputText _label="Input Text"></KernInputText>

					<KernInputText
						_hint="Input mit versteckten Label, dafür mit Hint!"
						_error="Ich bin eine Fehlermeldung!"
						_placeholder="wird bei laden der Seite validiert"
						_icons="{'left': { 'icon': 'material-symbols-outlined search'}}"
						_on={{
							onBlur: console.log,
							onChange: console.log,
							onClick: console.log,
							onFocus: console.log,
						}}
						_hideLabel
						_required
						_type="search"
						_touched
						_label="Suche"
					/>
					<KernInputText _accessKey="V" _placeholder="Pflichtfeld ihne error Text" _label="Vorname (text)" _required />
					<KernInputText _placeholder="Placeholder" _label="Suche (search)" _type="search" />
					<KernInputText _placeholder="Placeholder" _error="Ich bin eine Fehlermeldung!" _touched _type="url" _label="URL (url)" />
					<KernInputText _placeholder="Placeholder" _type="tel" _label="Telefon (tel)" />
					<KernInputText _placeholder="Placeholder" _readOnly _label="Vorname (text, readonly)" />
					<KernInputText _value="Value" _readOnly _label="Vorname (text, readonly)" />
					<KernInputText _value="Value" _disabled _label="Vorname (text, disabled)" />
					<KernInputText _placeholder="Placeholder" _disabled _label="Vorname (text, disabled)" />
					<KernInputText _value="Value" _hint="hint Text" _disabled _label="Vorname (text, disabled)" />
				</div>
			</div>
		);
	}
}
export default InputText;
