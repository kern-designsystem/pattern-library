import { KernIcon, KernButton, KernHeading, KernBadge } from '@kern-ux/react';
import React from 'react';

class Icon extends React.Component {
	render() {
		return (
			<div className="component-section" id="icon">
				<header className="section-header">
					<KernHeading _level="2" _label="Icons"></KernHeading>
					<div className="header-icons">
						<div className="component-row">
							<KernBadge _label="Style" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="React Adapter" _status="success" _icons="material-symbols-outlined filled small badge_check_circle"></KernBadge>
							<KernBadge _label="Last update: 22.12.23" _status="info" _icons="material-symbols-outlined filled small badge_info"></KernBadge>
						</div>
					</div>
				</header>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Link"></KernHeading>
					<div className="component-row">
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled arrow_forward"
							_variant="tertiary"
							_icons="material-symbols-rounded filled arrow_forward"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled arrow_back"
							_variant="tertiary"
							_icons="material-symbols-rounded filled arrow_back"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled open_in_new"
							_variant="tertiary"
							_icons="material-symbols-rounded filled open_in_new"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled download"
							_variant="tertiary"
							_icons="material-symbols-rounded filled download"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled logout"
							_variant="tertiary"
							_icons="material-symbols-rounded filled logout"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled checklist"
							_variant="tertiary"
							_icons="material-symbols-rounded filled checklist"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled mail"
							_variant="tertiary"
							_icons="material-symbols-rounded filled mail"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled edit"
							_variant="tertiary"
							_icons="material-symbols-rounded filled edit"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled sign_language"
							_variant="tertiary"
							_icons="material-symbols-rounded filled sign_language"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled easy_language"
							_variant="tertiary"
							_icons="material-symbols-rounded filled easy_language"
						></KernButton>
					</div>
					<KernHeading className="variant-label" _level="3" _label="Button"></KernHeading>
					<div className="component-row">
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled arrow_forward"
							_variant="tertiary"
							_icons="material-symbols-rounded filled arrow_forward"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled arrow_back"
							_variant="tertiary"
							_icons="material-symbols-rounded filled arrow_back"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled autorenew"
							_variant="tertiary"
							_icons="material-symbols-rounded filled autorenew"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled add"
							_variant="tertiary"
							_icons="material-symbols-rounded filled add"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled close"
							_variant="tertiary"
							_icons="material-symbols-rounded filled close"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled download"
							_variant="tertiary"
							_icons="material-symbols-rounded filled download"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled delete"
							_variant="tertiary"
							_icons="material-symbols-rounded filled delete"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled search"
							_variant="tertiary"
							_icons="material-symbols-rounded filled search"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled question_mark"
							_variant="tertiary"
							_icons="material-symbols-rounded filled question_mark"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled more_vert"
							_variant="tertiary"
							_icons="material-symbols-rounded filled more_vert"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled mail"
							_variant="tertiary"
							_icons="material-symbols-rounded filled mail"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled edit"
							_variant="tertiary"
							_icons="material-symbols-rounded filled edit"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled content_copy"
							_variant="tertiary"
							_icons="material-symbols-rounded filled content_copy"
						></KernButton>
					</div>
					<KernHeading className="variant-label" _level="3" _label="Status"></KernHeading>
					<div className="component-row">
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled check_circle"
							_variant="tertiary"
							_icons="material-symbols-rounded filled check_circle"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled info"
							_variant="tertiary"
							_icons="material-symbols-rounded filled info"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled emergency_home"
							_variant="tertiary"
							_icons="material-symbols-rounded filled emergency_home"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled warning"
							_variant="tertiary"
							_icons="material-symbols-rounded filled warning"
						></KernButton>
					</div>
					<KernHeading className="variant-label" _level="3" _label="Form"></KernHeading>
					<div className="component-row">
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled calendar_today"
							_variant="tertiary"
							_icons="material-symbols-rounded filled calendar_today"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled visibility"
							_variant="tertiary"
							_icons="material-symbols-rounded filled visibility"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled visibility_off"
							_variant="tertiary"
							_icons="material-symbols-rounded filled visibility_off"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled expand_more"
							_variant="tertiary"
							_icons="material-symbols-rounded filled expand_more"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled expand_less"
							_variant="tertiary"
							_icons="material-symbols-rounded filled expand_less"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled check"
							_variant="tertiary"
							_icons="material-symbols-rounded filled check"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled drive_folder_upload"
							_variant="tertiary"
							_icons="material-symbols-rounded filled drive_folder_upload"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled draft"
							_variant="tertiary"
							_icons="material-symbols-rounded filled draft"
						></KernButton>
					</div>
					<KernHeading className="variant-label" _level="3" _label="Navigation"></KernHeading>
					<div className="component-row">
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled home"
							_variant="tertiary"
							_icons="material-symbols-rounded filled home"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled keyboard_arrow_right"
							_variant="tertiary"
							_icons="material-symbols-rounded filled keyboard_arrow_right"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled keyboard_arrow_left"
							_variant="tertiary"
							_icons="material-symbols-rounded filled keyboard_arrow_left"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled keyboard_double_arrow_right"
							_variant="tertiary"
							_icons="material-symbols-rounded filled keyboard_double_arrow_right"
						></KernButton>
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled keyboard_double_arrow_left"
							_variant="tertiary"
							_icons="material-symbols-rounded filled keyboard_double_arrow_left"
						></KernButton>
					</div>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="LARGE Icons 48dp"></KernHeading>
					<div className="component-row">
						<KernButton
							_hide-label="true"
							_label="material-symbols-rounded filled large drive_folder_upload"
							_variant="tertiary"
							_icons="material-symbols-rounded filled large drive_folder_upload"
						></KernButton>
					</div>
				</div>

				{/* <div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Rounded"></KernHeading>
					<div className="component-row">
						<KernButton _hide-label="true" _label="material-symbols-rounded filled draft" _variant="tertiary" _icons="material-symbols-rounded draft"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled arrow_forward" _variant="tertiary" _icons="material-symbols-rounded arrow_forward"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled arrow_back" _variant="tertiary" _icons="material-symbols-rounded arrow_back"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled expand_more" _variant="tertiary" _icons="material-symbols-rounded expand_more"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled delete" _variant="tertiary" _icons="material-symbols-rounded delete"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled expand_less" _variant="tertiary" _icons="material-symbols-rounded expand_less"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled open_in_new" _variant="tertiary" _icons="material-symbols-rounded open_in_new"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled download" _variant="tertiary" _icons="material-symbols-rounded download"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled mail" _variant="tertiary" _icons="material-symbols-rounded mail"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled search" _variant="tertiary" _icons="material-symbols-rounded search"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled autorenew" _variant="tertiary" _icons="material-symbols-rounded autorenew"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled add" _variant="tertiary" _icons="material-symbols-rounded add"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled done" _variant="tertiary" _icons="material-symbols-rounded done"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled close" _variant="tertiary" _icons="material-symbols-rounded close"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled more_vert" _variant="tertiary" _icons="material-symbols-rounded more_vert"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled warning" _variant="tertiary" _icons="material-symbols-rounded warning"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled emergency_home" _variant="tertiary" _icons="material-symbols-rounded emergency_home"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled check_circle" _variant="tertiary" _icons="material-symbols-rounded check_circle"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled all_inclusive" _variant="tertiary" _icons="material-symbols-rounded all_inclusive"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled chat" _variant="tertiary" _icons="material-symbols-rounded chat"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled check" _variant="tertiary" _icons="material-symbols-rounded check"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled home" _variant="tertiary" _icons="material-symbols-rounded home"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled info" _variant="tertiary" _icons="material-symbols-rounded info"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled keyboard_arrow_down" _variant="tertiary" _icons="material-symbols-rounded keyboard_arrow_down"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled keyboard_arrow_left" _variant="tertiary" _icons="material-symbols-rounded keyboard_arrow_left"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled keyboard_arrow_right" _variant="tertiary" _icons="material-symbols-rounded keyboard_arrow_right"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled keyboard_arrow_up" _variant="tertiary" _icons="material-symbols-rounded keyboard_arrow_up"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled keyboard_double_arrow_left" _variant="tertiary" _icons="material-symbols-rounded keyboard_double_arrow_left"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled keyboard_double_arrow_right" _variant="tertiary" _icons="material-symbols-rounded keyboard_double_arrow_right"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled library_add_check" _variant="tertiary" _icons="material-symbols-rounded library_add_check"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled remove" _variant="tertiary" _icons="material-symbols-rounded remove"></KernButton>
					</div>
				</div>

				<div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Outlined"></KernHeading>
					<div className="component-row">
						<KernButton _hide-label="true" _label="material-symbols-outlined draft" _variant="tertiary" _icons="material-symbols-outlined draft"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined arrow_forward" _variant="tertiary" _icons="material-symbols-outlined arrow_forward"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined arrow_back" _variant="tertiary" _icons="material-symbols-outlined arrow_back"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined expand_more" _variant="tertiary" _icons="material-symbols-outlined expand_more"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined delete" _variant="tertiary" _icons="material-symbols-outlined delete"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined expand_less" _variant="tertiary" _icons="material-symbols-outlined expand_less"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined open_in_new" _variant="tertiary" _icons="material-symbols-outlined open_in_new"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined download" _variant="tertiary" _icons="material-symbols-outlined download"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined mail" _variant="tertiary" _icons="material-symbols-outlined mail"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined search" _variant="tertiary" _icons="material-symbols-outlined search"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined autorenew" _variant="tertiary" _icons="material-symbols-outlined autorenew"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined add" _variant="tertiary" _icons="material-symbols-outlined add"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined done" _variant="tertiary" _icons="material-symbols-outlined done"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined close" _variant="tertiary" _icons="material-symbols-outlined close"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined more_vert" _variant="tertiary" _icons="material-symbols-outlined more_vert"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined warning" _variant="tertiary" _icons="material-symbols-outlined warning"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined emergency_home" _variant="tertiary" _icons="material-symbols-outlined emergency_home"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined check_circle" _variant="tertiary" _icons="material-symbols-outlined check_circle"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined all_inclusive" _variant="tertiary" _icons="material-symbols-outlined all_inclusive"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined chat" _variant="tertiary" _icons="material-symbols-outlined chat"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined check" _variant="tertiary" _icons="material-symbols-outlined check"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined home" _variant="tertiary" _icons="material-symbols-outlined home"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined info" _variant="tertiary" _icons="material-symbols-outlined info"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined keyboard_arrow_down" _variant="tertiary" _icons="material-symbols-outlined keyboard_arrow_down"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined keyboard_arrow_left" _variant="tertiary" _icons="material-symbols-outlined keyboard_arrow_left"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined keyboard_arrow_right" _variant="tertiary" _icons="material-symbols-outlined keyboard_arrow_right"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined keyboard_arrow_up" _variant="tertiary" _icons="material-symbols-outlined keyboard_arrow_up"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined keyboard_double_arrow_left" _variant="tertiary" _icons="material-symbols-outlined keyboard_double_arrow_left"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined keyboard_double_arrow_right" _variant="tertiary" _icons="material-symbols-outlined keyboard_double_arrow_right"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined library_add_check" _variant="tertiary" _icons="material-symbols-outlined library_add_check"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined remove" _variant="tertiary" _icons="material-symbols-outlined remove"></KernButton>
					</div>
				</div> */}

				{/* <div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Rounded Filled"></KernHeading>
					<div className="component-row">
						<KernButton _hide-label="true" _label="material-symbols-rounded filled draft" _variant="tertiary" _icons="material-symbols-rounded filled draft"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled arrow_forward" _variant="tertiary" _icons="material-symbols-rounded filled arrow_forward"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled arrow_back" _variant="tertiary" _icons="material-symbols-rounded filled arrow_back"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled expand_more" _variant="tertiary" _icons="material-symbols-rounded filled expand_more"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled delete" _variant="tertiary" _icons="material-symbols-rounded filled delete"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled expand_less" _variant="tertiary" _icons="material-symbols-rounded filled expand_less"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled open_in_new" _variant="tertiary" _icons="material-symbols-rounded filled open_in_new"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled download" _variant="tertiary" _icons="material-symbols-rounded filled download"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled mail" _variant="tertiary" _icons="material-symbols-rounded filled mail"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled search" _variant="tertiary" _icons="material-symbols-rounded filled search"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled autorenew" _variant="tertiary" _icons="material-symbols-rounded filled autorenew"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled add" _variant="tertiary" _icons="material-symbols-rounded filled add"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled done" _variant="tertiary" _icons="material-symbols-rounded filled done"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled close" _variant="tertiary" _icons="material-symbols-rounded filled close"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled more_vert" _variant="tertiary" _icons="material-symbols-rounded filled more_vert"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled warning" _variant="tertiary" _icons="material-symbols-rounded filled warning"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled emergency_home" _variant="tertiary" _icons="material-symbols-rounded filled emergency_home"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled check_circle" _variant="tertiary" _icons="material-symbols-rounded filled check_circle"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled all_inclusive" _variant="tertiary" _icons="material-symbols-rounded filled all_inclusive"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled chat" _variant="tertiary" _icons="material-symbols-rounded filled chat"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled check" _variant="tertiary" _icons="material-symbols-rounded filled check"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled home" _variant="tertiary" _icons="material-symbols-rounded filled home"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled info" _variant="tertiary" _icons="material-symbols-rounded filled info"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled keyboard_arrow_down" _variant="tertiary" _icons="material-symbols-rounded filled keyboard_arrow_down"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled keyboard_arrow_left" _variant="tertiary" _icons="material-symbols-rounded filled keyboard_arrow_left"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled keyboard_arrow_right" _variant="tertiary" _icons="material-symbols-rounded filled keyboard_arrow_right"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled keyboard_arrow_up" _variant="tertiary" _icons="material-symbols-rounded filled keyboard_arrow_up"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled keyboard_double_arrow_left" _variant="tertiary" _icons="material-symbols-rounded filled keyboard_double_arrow_left"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled keyboard_double_arrow_right" _variant="tertiary" _icons="material-symbols-rounded filled keyboard_double_arrow_right"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled library_add_check" _variant="tertiary" _icons="material-symbols-rounded filled library_add_check"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled remove" _variant="tertiary" _icons="material-symbols-rounded filled remove"></KernButton>
					</div>
				</div> */}

				{/* <div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Outlined Filled"></KernHeading>
					<div className="component-row">
						<KernButton _hide-label="true" _label="material-symbols-outlined filled draft" _variant="tertiary" _icons="material-symbols-outlined filled draft"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled arrow_forward" _variant="tertiary" _icons="material-symbols-outlined filled arrow_forward"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled arrow_back" _variant="tertiary" _icons="material-symbols-outlined filled arrow_back"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled expand_more" _variant="tertiary" _icons="material-symbols-outlined filled expand_more"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled delete" _variant="tertiary" _icons="material-symbols-outlined filled delete"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled expand_less" _variant="tertiary" _icons="material-symbols-outlined filled expand_less"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled open_in_new" _variant="tertiary" _icons="material-symbols-outlined filled open_in_new"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled download" _variant="tertiary" _icons="material-symbols-outlined filled download"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled mail" _variant="tertiary" _icons="material-symbols-outlined filled mail"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled search" _variant="tertiary" _icons="material-symbols-outlined filled search"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled autorenew" _variant="tertiary" _icons="material-symbols-outlined filled autorenew"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled add" _variant="tertiary" _icons="material-symbols-outlined filled add"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled done" _variant="tertiary" _icons="material-symbols-outlined filled done"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled close" _variant="tertiary" _icons="material-symbols-outlined filled close"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled more_vert" _variant="tertiary" _icons="material-symbols-outlined filled more_vert"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled warning" _variant="tertiary" _icons="material-symbols-outlined filled warning"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled emergency_home" _variant="tertiary" _icons="material-symbols-outlined filled emergency_home"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled check_circle" _variant="tertiary" _icons="material-symbols-outlined filled check_circle"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled all_inclusive" _variant="tertiary" _icons="material-symbols-outlined filled all_inclusive"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled chat" _variant="tertiary" _icons="material-symbols-outlined filled chat"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled check" _variant="tertiary" _icons="material-symbols-outlined filled check"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled home" _variant="tertiary" _icons="material-symbols-outlined filled home"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled info" _variant="tertiary" _icons="material-symbols-outlined filled info"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled keyboard_arrow_down" _variant="tertiary" _icons="material-symbols-outlined filled keyboard_arrow_down"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled keyboard_arrow_left" _variant="tertiary" _icons="material-symbols-outlined filled keyboard_arrow_left"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled keyboard_arrow_right" _variant="tertiary" _icons="material-symbols-outlined filled keyboard_arrow_right"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled keyboard_arrow_up" _variant="tertiary" _icons="material-symbols-outlined filled keyboard_arrow_up"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled keyboard_double_arrow_left" _variant="tertiary" _icons="material-symbols-outlined filled keyboard_double_arrow_left"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled keyboard_double_arrow_right" _variant="tertiary" _icons="material-symbols-outlined filled keyboard_double_arrow_right"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled library_add_check" _variant="tertiary" _icons="material-symbols-outlined filled library_add_check"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-outlined filled remove" _variant="tertiary" _icons="material-symbols-outlined filled remove"></KernButton>
					</div>
				</div> */}

				{/* <div className="component-col">
					<KernHeading className="variant-label" _level="3" _label="Rounded Filled LARGE"></KernHeading>
					<div className="component-row">
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large draft" _variant="tertiary" _icons="material-symbols-rounded filled large draft"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large arrow_forward" _variant="tertiary" _icons="material-symbols-rounded filled large arrow_forward"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large arrow_back" _variant="tertiary" _icons="material-symbols-rounded filled large arrow_back"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large expand_more" _variant="tertiary" _icons="material-symbols-rounded filled large expand_more"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large delete" _variant="tertiary" _icons="material-symbols-rounded filled large delete"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large expand_less" _variant="tertiary" _icons="material-symbols-rounded filled large expand_less"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large open_in_new" _variant="tertiary" _icons="material-symbols-rounded filled large open_in_new"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large download" _variant="tertiary" _icons="material-symbols-rounded filled large download"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large mail" _variant="tertiary" _icons="material-symbols-rounded filled large mail"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large search" _variant="tertiary" _icons="material-symbols-rounded filled large search"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large autorenew" _variant="tertiary" _icons="material-symbols-rounded filled large autorenew"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large add" _variant="tertiary" _icons="material-symbols-rounded filled large add"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large done" _variant="tertiary" _icons="material-symbols-rounded filled large done"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large close" _variant="tertiary" _icons="material-symbols-rounded filled large close"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large more_vert" _variant="tertiary" _icons="material-symbols-rounded filled large more_vert"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large warning" _variant="tertiary" _icons="material-symbols-rounded filled large warning"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large emergency_home" _variant="tertiary" _icons="material-symbols-rounded filled large emergency_home"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large check_circle" _variant="tertiary" _icons="material-symbols-rounded filled large check_circle"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large all_inclusive" _variant="tertiary" _icons="material-symbols-rounded filled large all_inclusive"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large chat" _variant="tertiary" _icons="material-symbols-rounded filled large chat"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large check" _variant="tertiary" _icons="material-symbols-rounded filled large check"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large home" _variant="tertiary" _icons="material-symbols-rounded filled large home"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large info" _variant="tertiary" _icons="material-symbols-rounded filled large info"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large keyboard_arrow_down" _variant="tertiary" _icons="material-symbols-rounded filled large keyboard_arrow_down"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large keyboard_arrow_left" _variant="tertiary" _icons="material-symbols-rounded filled large keyboard_arrow_left"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large keyboard_arrow_right" _variant="tertiary" _icons="material-symbols-rounded filled large keyboard_arrow_right"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large keyboard_arrow_up" _variant="tertiary" _icons="material-symbols-rounded filled large keyboard_arrow_up"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large keyboard_double_arrow_left" _variant="tertiary" _icons="material-symbols-rounded filled large keyboard_double_arrow_left"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large keyboard_double_arrow_right" _variant="tertiary" _icons="material-symbols-rounded filled large keyboard_double_arrow_right"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large library_add_check" _variant="tertiary" _icons="material-symbols-rounded filled large library_add_check"></KernButton>
						<KernButton _hide-label="true" _label="material-symbols-rounded filled large remove" _variant="tertiary" _icons="material-symbols-rounded filled large remove"></KernButton>
					</div>
				</div> */}

				{/*
				// <div className="component-col">
				// 	<KernHeading className="variant-label" _level="3" _label="Outlined"></KernHeading>
				// 	<div className="component-row">
				// 		<KernIcon _icons="material-symbols-outlined draft" _label="Draft"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined arrow_forward" _aria-label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined arrow_back" _aria-label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined expand_more" _laria-abel="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined delete" _aria-label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined expand_less" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined open_in_new" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined download" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined mail" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined search" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined autorenew" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined add" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined done" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined close" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined more_vert" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined warning" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined emergency_home" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined check_circle" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined all_inclusive" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined chat" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined check" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined home" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined info" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined keyboard_arrow_down" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined keyboard_arrow_left" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined keyboard_arrow_right" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined keyboard_arrow_up" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined keyboard_double_arrow_left" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined keyboard_double_arrow_right" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined library_add_check" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined remove" _label="Ich bin ein Icon"></KernIcon>
				// 	</div>
				// </div> */}

				{/* // <div className="component-col">
				// 	<KernHeading className="variant-label" _level="3" _label="Rounded Filled"></KernHeading>
				// 	<div className="component-row">
				// 		<KernIcon _icons="material-symbols-rounded filled draft" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled arrow_forward" _aria-label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled arrow_back" _aria-label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled expand_more" _laria-abel="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled delete" _aria-label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled expand_less" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled open_in_new" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled download" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled mail" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled search" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled autorenew" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled add" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled done" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled close" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled more_vert" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled warning" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled emergency_home" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled check_circle" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled all_inclusive" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled chat" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled check" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled home" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled info" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled keyboard_arrow_down" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled keyboard_arrow_left" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled keyboard_arrow_right" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled keyboard_arrow_up" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled keyboard_double_arrow_left" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled keyboard_double_arrow_right" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled library_add_check" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-rounded filled remove" _label="Ich bin ein Icon"></KernIcon>
				// 	</div>
				// </div>

				// <div className="component-col">
				// 	<KernHeading className="variant-label" _level="3" _label="Outlined Filled"></KernHeading>
				// 	<div className="component-row">
				// 		<KernIcon _icons="material-symbols-outlined filled draft" _label="Draft"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled arrow_forward" _aria-label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled arrow_back" _aria-label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled expand_more" _laria-abel="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled delete" _aria-label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled expand_less" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled open_in_new" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled download" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled mail" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled search" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled autorenew" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled add" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled done" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled close" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled more_vert" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled warning" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled emergency_home" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled check_circle" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled all_inclusive" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled chat" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled check" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled home" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled info" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled keyboard_arrow_down" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled keyboard_arrow_left" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled keyboard_arrow_right" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled keyboard_arrow_up" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled keyboard_double_arrow_left" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled keyboard_double_arrow_right" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled library_add_check" _label="Ich bin ein Icon"></KernIcon>
				// 		<KernIcon _icons="material-symbols-outlined filled remove" _label="Ich bin ein Icon"></KernIcon>
				// 	</div>
				// </div> */}
			</div>
		);
	}
}
export default Icon;
