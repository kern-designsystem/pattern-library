function openModal(modal, trigger) {
	return function() {
		modal._activeElement = trigger;
	}
}

function closeModal(modal) {
	return function() {
		modal._activeElement = null;
	}
}

function setModalFunctions(triggerSelector, modalSelector, closeSelector) {
	const trigger = document.querySelector(triggerSelector);
	const modal = document.querySelector(modalSelector);
	const close = document.querySelector(closeSelector);
	if (modal) {
		if (trigger) {
			trigger._on = {
				onClick: openModal(modal, trigger),
			};
		}
		modal._on = {
			onClose: closeModal(modal),
		};
	}
	if (close) {
		close._on = {
			onClick: closeModal(modal),
		};
	}
}

setModalFunctions('#modal-open-1', '#modal-1', '#modal-1-cancel');
setModalFunctions('#modal-open-2', '#modal-2', '#modal-2-cancel');
setModalFunctions('#modal-open-3', '#modal-3');
