import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, Prop, State, Watch } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredStates, States, KernOrientation } from './schema';
import { watchString } from '../../utils/prop.validators';

@Component({
	tag: 'kern-fieldset',
	shadow: false,
	styleUrls: {
		default: './style.scss',
	},
})
export class KernFieldset implements Generic.Element.ComponentApi<NonNullable<unknown>, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Gibt an, welche Custom-Class übergeben werden soll, wenn _variant="custom" gesetzt ist.
	 */
	@Prop() public _customClass?: string;

	/**
	 * Gibt an, welcher Text als Überschrift des Fieldsets angezeigt werden soll.
	 */
	@Prop() public _legend?: string;

	/**
	 * Defines whether the orientation of the component is horizontal or vertical.
	 */
	@Prop() public _orientation?: KernOrientation = 'vertical';

	@State() public state: States = {
		_legend: '',
		_orientation: 'vertical',
	};

	@Watch('_customClass')
	public validateCustomClass(value?: string): void {
		watchString(this, '_customClass', value, {
			defaultValue: undefined,
		});
	}

	@Watch('_orientation')
	public validateOrientation(value?: KernOrientation): void {
		watchString(this, '_orientation', value, {
			defaultValue: 'vertical',
		});
	}

	@Watch('_legend')
	public validateLegend(value?: string): void {
		watchString(this, '_legend', value, {
			defaultValue: undefined,
		});
	}

	public componentWillLoad(): void {
		this.validateCustomClass(this._customClass);
		this.validateLegend(this._legend);
		this.validateOrientation(this._orientation);
	}

	public render(): JSX.Element {
		return (
			<fieldset
				class={{
					[this.state._customClass as string]: typeof this.state._customClass === 'string' && this.state._customClass.length > 0,
					[this.state._orientation as KernOrientation]: typeof this.state._orientation === 'string',
				}}
			>
				{this.state._legend ? <legend>{this.state._legend}</legend> : ''}
				<slot></slot>
			</fieldset>
		);
	}
}
