# kern-divider

<!-- Auto Generated Below -->

## Properties

| Property       | Attribute       | Description                                                                              | Type                   | Default     |
| -------------- | --------------- | ---------------------------------------------------------------------------------------- | ---------------------- | ----------- |
| `_ariaHidden`  | `_aria-hidden`  | Gibt an, ob das Element für Screenreader versteckt werden soll.                          | `boolean \| undefined` | `undefined` |
| `_customClass` | `_custom-class` | Gibt an, welche Custom-Class übergeben werden soll, wenn \_variant="custom" gesetzt ist. | `string \| undefined`  | `undefined` |

---
