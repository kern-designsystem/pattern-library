import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, Prop, State, Watch, Host } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import { watchString } from '../../utils/prop.validators';

@Component({
	tag: 'kern-divider',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernDivider implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Gibt an, welche Custom-Class übergeben werden soll, wenn _variant="custom" gesetzt ist.
	 */
	@Prop() public _customClass?: string;

	/**
	 * Gibt an, ob das Element für Screenreader versteckt werden soll.
	 */
	@Prop() public _ariaHidden?: boolean;

	@State() public state: States = {};

	@Watch('_customClass')
	public validateCustomClass(value?: string): void {
		watchString(this, '_customClass', value, {
			defaultValue: undefined,
		});
	}

	@Watch('_ariaHidden')
	public validateAriaHidden(value?: boolean): void {
		watchString(this, '_ariaHidden', value?.toString(), {
			defaultValue: 'false',
		});
	}

	public componentWillLoad(): void {
		this.validateCustomClass(this._customClass);
	}

	public render(): JSX.Element {
		return (
			<Host>
				<hr class={this.state._customClass} aria-hidden={this._ariaHidden ? 'true' : 'false'} />
			</Host>
		);
	}
}
