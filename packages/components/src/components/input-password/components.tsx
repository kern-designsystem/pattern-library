import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, State, Prop, Watch, Host, Element, forceUpdate } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import { KernIconProp } from '../button/schema';
import { ButtonProps, IdPropType, InputTypeOnDefault, InputTypeOnOff, LabelWithExpertSlotPropType, Stringified, TooltipAlignPropType } from '@public-ui/schema';

@Component({
	tag: 'kern-input-password',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernInputPassword implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	private passwordRef?: HTMLInputElement;
	private inputElement?: HTMLInputElement;
	@Element() el!: HTMLElement;
	/**
	 * Definiert, welche Tastenkombination verwendet werden kann, um das interaktive Element des Komponenten auszulösen oder zu fokussieren.
	 */
	@Prop() public _accessKey?: string;

	/**
	 * Legt fest, ob das Eingabefeld automatisch vervollständigt werden kann.
	 */
	@Prop() public _autoComplete?: InputTypeOnOff;

	/**
	 * Macht das Element nicht fokussierbar und ignoriert alle Ereignisse.
	 */
	@Prop() public _disabled?: boolean = false;

	/**
	 * Definiert den Text der Fehlermeldung.
	 */
	@Prop() public _error?: string;

	/**
	 * Zeigt die Zeichenanzahl an der unteren Grenze der Eingabe an.
	 */
	@Prop() public _hasCounter?: boolean = false;

	/**
	 * Versteckt die Fehlermeldung, lässt sie aber im DOM für das aria-describedby der Eingabe.
	 */
	@Prop({ mutable: true, reflect: true }) public _hideError?: boolean = false;

	/**
	 * Versteckt die Beschriftung standardmäßig und zeigt den Beschriftungstext mit einem Tooltip an, wenn das interaktive Element fokussiert ist oder die Maus darüber ist.
	 */
	@Prop() public _hideLabel?: boolean = false;

	/**
	 * Definiert den Hinweistext.
	 */
	@Prop() public _hint?: string = '';

	/**
	 * Gibt die aktuelle Länge des Eingabewerts an.
	 */
	@Prop() readonly _currentLength?: number;

	/**
	 * Definiert die interne ID des primären Komponentenelements.
	 */
	@Prop() public _id?: IdPropType;

	/**
	 * Definiert das sichtbare oder semantische Label der Komponente (z.B. aria-label, label, Überschrift, Beschriftung, Zusammenfassung, etc.). Setzen Sie auf `false`, um den Experten-Slot zu aktivieren.
	 */
	@Prop() public _label!: LabelWithExpertSlotPropType;

	/**
	 * Definiert die maximale Anzahl von Eingabezeichen.
	 */
	@Prop() public _maxLength?: number;

	/**
	 * Definiert den technischen Namen eines Eingabefelds.
	 */
	@Prop() public _name?: string;

	/**
	 * Gibt die EventCallback-Funktionen für das Input-Event an.
	 */
	@Prop() public _on?: InputTypeOnDefault;

	/**
	 * Setzt die Iconklasse (z.B.: `_icon="material-symbols-outlined home"`).
	 */
	@Prop() public _icons?: KernIconProp;

	/**
	 * Definiert den Platzhalter für das Eingabefeld. Wird angezeigt, wenn kein Wert vorhanden ist.
	 */
	@Prop() public _placeholder?: string;

	/**
	 * Macht das Eingabeelement schreibgeschützt.
	 */
	@Prop() public _readOnly?: boolean = false;

	/**
	 * Macht das Eingabeelement erforderlich.
	 */
	@Prop() public _required?: boolean = false;

	/**
	 * Definiert, wo der Tooltip vorzugsweise angezeigt werden soll: oben, rechts, unten oder links.
	 */
	@Prop() public _tooltipAlign?: TooltipAlignPropType = 'top';

	/**
	 * Zeigt an, ob die Eingabe von einem Benutzer berührt wurde.
	 */
	@Prop({ mutable: true, reflect: true }) public _touched?: boolean = false;

	/**
	 * Defines the value of the input.
	 */
	@Prop() public _value?: string;

	/**
	 * Erlaubt das Hinzufügen einer Schaltfläche mit einer beliebigen Aktion innerhalb des Elements.
	 */
	@Prop() public _smartButton?: Stringified<ButtonProps>;

	/**
	 * Erlaubt das Hinzufügen einer Schaltfläche, um das Passwort anzuzeigen oder zu verbergen.
	 */
	@Prop() public _hasPasswordToggle?: boolean = false;

	// States
	@State() public state: States = {
		_currentLength: 0,
		_hideError: false,
		_label: '', // ⚠ required
		_icons: {},
		_hasPasswordToggle: false,
		_isPasswordVisible: false,
		_smartButton: undefined,
	};

	@Watch('_icons')
	public validateIcons(value?: KernIconProp): void {
		if (typeof value === 'string') {
			this.state._icons = value;
		}
	}
	@Watch('_hasPasswordToggle')
	public validateHasPasswordToggle(value?: boolean): void {
		if (typeof value === 'boolean') {
			this.state._hasPasswordToggle = value;
		}
	}

	@Watch('_label')
	public validateLabel(value?: string): void {
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}

	@Watch('_isPasswordVisible')
	public validateIsPasswordVisible(value?: boolean): void {
		if (typeof value === 'boolean') {
			this.state._isPasswordVisible = value;
		}
	}

	@Watch('_smartButton')
	public validateSmartButton(value?: Stringified<ButtonProps>): void {
		if (typeof value === 'object') {
			this.state._smartButton = value;
		}
	}

	public componentWillLoad(): void {
		this.validateIcons(this._icons);
		this.validateLabel(this._label);
		this.validateHasPasswordToggle(this._hasPasswordToggle);
	}

	componentDidLoad() {
		this.inputElement = this.passwordRef?.shadowRoot?.querySelector('input') ?? undefined;
	}

	public handleTogglePasswordClick = (): void => {
		this.state._isPasswordVisible = !this.state._isPasswordVisible;
		if (this.inputElement) {
			this.inputElement.type = this.state._isPasswordVisible ? 'text' : 'password';
		}
		forceUpdate(this);
	};

	public render(): JSX.Element {
		return (
			<Host>
				<kol-input-password
					class={{
						'hide-label': !!this._hideLabel,
						password: true,
					}}
					_accessKey={this._accessKey}
					_disabled={this._disabled}
					_error={this._error}
					_hasCounter={this._hasCounter}
					_hideError={this._hideError}
					_hideLabel={this._hideLabel}
					_hint={this._hint}
					_icons={this.state._icons}
					_id={this._id}
					_label={this.state._label}
					_maxLength={this._maxLength}
					_readOnly={this._readOnly}
					_required={this._required}
					_smartButton={
						this.state?._hasPasswordToggle
							? {
									_icons: {
										right: {
											icon: this.state._isPasswordVisible ? 'material-symbols-rounded filled visibility_off' : 'material-symbols-rounded filled visibility',
										},
									},
									_hideLabel: true,
									_label: `Passwort ${this.state._isPasswordVisible ? 'ausblenden' : 'einblenden'}`,
									_on: {
										onClick: this.handleTogglePasswordClick,
									},
								}
							: undefined
					}
					_tooltipAlign={this._tooltipAlign}
					_touched={this._touched}
					_name={this._name}
					_value={this._value}
					ref={(el: HTMLInputElement) => (this.passwordRef = el)}
				></kol-input-password>
			</Host>
		);
	}
}
