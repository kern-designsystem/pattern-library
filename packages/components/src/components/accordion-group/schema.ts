import { Generic } from 'adopted-style-sheets';
/**
 * API
 */
export type RequiredProps = NonNullable<unknown>;
export type OptionalProps = NonNullable<unknown>;

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = RequiredProps;
export type OptionalStates = OptionalProps;

export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
