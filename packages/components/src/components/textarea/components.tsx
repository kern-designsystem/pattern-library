import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, State, Prop, Watch } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import { KernIconProp } from '../button/schema';
import {
	ButtonProps,
	IdPropType,
	InputTextType,
	InputTypeOnDefault,
	InputTypeOnOff,
	LabelWithExpertSlotPropType,
	NamePropType,
	Stringified,
	SuggestionsPropType,
	SyncValueBySelectorPropType,
	TooltipAlignPropType,
} from '@public-ui/schema';

@Component({
	tag: 'kern-textarea',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernTextarea implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Definiert, welche Tastenkombination verwendet werden kann, um das interaktive Element des Komponenten auszulösen oder zu fokussieren.
	 */
	@Prop() public _accessKey?: string;

	/**
	 * Definiert, ob die Bildschirmleser die Benachrichtigung vorlesen sollen.
	 */
	@Prop({ mutable: true, reflect: true }) public _alert?: boolean = true;

	/**
	 * Legt fest, ob das Eingabefeld automatisch vervollständigt werden kann.
	 */
	@Prop() public _autoComplete?: InputTypeOnOff;

	/**
	 * Macht das Element nicht fokussierbar und ignoriert alle Ereignisse.
	 */
	@Prop() public _disabled?: boolean = false;

	/**
	 * Definiert den Text der Fehlermeldung.
	 */
	@Prop() public _error?: string;

	/**
	 * Zeigt die Zeichenanzahl an der unteren Grenze der Eingabe an.
	 */
	@Prop() public _hasCounter?: boolean = false;

	/**
	 * Versteckt die Fehlermeldung, lässt sie aber im DOM für das aria-describedby der Eingabe.
	 */
	@Prop({ mutable: true, reflect: true }) public _hideError?: boolean = false;

	/**
	 * Versteckt die Beschriftung standardmäßig und zeigt den Beschriftungstext mit einem Tooltip an, wenn das interaktive Element fokussiert ist oder die Maus darüber ist.
	 */
	@Prop() public _hideLabel?: boolean = false;

	/**
	 * Definiert den Hinweistext.
	 */
	@Prop() public _hint?: string = '';

	/**
	 * Definiert die Klassennamen des Symbols (z.B. `_icons="material-symbols filled rounded search"`).
	 */
	@Prop() public _icons?: KernIconProp;

	/**
	 * Definiert die interne ID des primären Komponentenelements.
	 */
	@Prop() public _id?: IdPropType;

	/**
	 * Definiert das sichtbare oder semantische Label der Komponente (z.B. aria-label, label, Überschrift, Beschriftung, Zusammenfassung, etc.). Setzen Sie auf `false`, um den Experten-Slot zu aktivieren.
	 */
	@Prop() public _label!: LabelWithExpertSlotPropType;

	/**
	 * Defines the maximum number of input characters.
	 */
	@Prop() public _maxLength?: number;

	/**
	 * Definiert den technischen Namen eines Eingabefelds.
	 */
	@Prop() public _name?: NamePropType;

	/**
	 * Gibt die EventCallback-Funktionen für das Input-Event an.
	 */
	@Prop() public _on?: InputTypeOnDefault;

	/**
	 * Defines a validation pattern for the input field.
	 */
	@Prop() public _pattern?: string;

	/**
	 * Gibt den Placeholder des Inputs an.
	 */
	@Prop() public _placeholder?: string;

	/**
	 * Macht das Eingabeelement schreibgeschützt.
	 */
	@Prop() public _readOnly?: boolean = false;

	/**
	 * Macht das Eingabeelement erforderlich.
	 */
	@Prop() public _required?: boolean = false;

	/**
	 * Suggestions to provide for the input.
	 */
	@Prop() public _suggestions?: SuggestionsPropType;

	/**
	 * Allows to add a button with an arbitrary action within the element (_hide-label only).
	 */
	@Prop() public _smartButton?: Stringified<ButtonProps>;

	/**
	 * Selektor zur Synchronisierung des Wertes mit einem anderen Eingabeelement.
	 */
	@Prop() public _syncValueBySelector?: SyncValueBySelectorPropType;

	/**
	 * Definiert, welchen Tab-Index das primäre Element der Komponente hat. (https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex)
	 */
	@Prop() public _tabIndex?: number;

	/**
	 * Definiert, wo der Tooltip vorzugsweise angezeigt werden soll: oben, rechts, unten oder links.
	 */
	@Prop() public _tooltipAlign?: TooltipAlignPropType = 'top';

	/**
	 * Zeigt an, ob die Eingabe von einem Benutzer berührt wurde.
	 */
	@Prop({ mutable: true, reflect: true }) public _touched?: boolean = false;

	/**
	 * Defines either the type of the component or of the components interactive element.
	 */
	@Prop() public _type?: InputTextType = 'text';

	/**
	 * Defines the value of the input.
	 */
	@Prop({ mutable: true }) public _value?: string;

	// States
	@State() public state: States = {
		_icons: {},
		_label: '',
		_hideLabel: false,
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}

	@Watch('_icons')
	public validateIcons(value?: KernIconProp): void {
		if (typeof value === 'string') {
			this.state._icons = value;
		}
	}

	public componentWillLoad(): void {
		this.validateLabel(this._label);
		this.validateIcons(this._icons);
	}

	public render(): JSX.Element {
		return (
			<kol-textarea
				class={{
					'hide-label': !!this._hideLabel,
					'has-counter': !!this._hasCounter,
				}}
				_placeholder={this._placeholder}
				_autoComplete={this._autoComplete}
				_accessKey={this._accessKey}
				_disabled={this._disabled}
				_error={this._error}
				_hideError={this._hideError}
				_hasCounter={this._hasCounter}
				_hideLabel={this._hideLabel}
				_hint={this._hint}
				_icons={this.state._icons}
				_id={this._id}
				_on={this._on}
				_label={this.state._label}
				_suggestions={this._suggestions}
				_maxLength={this._maxLength}
				_pattern={this._pattern}
				_readOnly={this._readOnly}
				_required={this._required}
				_smartButton={this._smartButton}
				_syncValueBySelector={this._syncValueBySelector}
				_tabIndex={this._tabIndex}
				_tooltipAlign={this._tooltipAlign}
				_touched={this._touched}
				_type={this._type}
				_name={this._name}
				_value={this._value}
			>
				<slot />
			</kol-textarea>
		);
	}
}
