# kern-accordion

<!-- Auto Generated Below -->

## Properties

| Property              | Attribute   | Description                                                                                                        | Type                                                                                 | Default     |
| --------------------- | ----------- | ------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------ | ----------- |
| `_disabled`           | `_disabled` | Makes the element not focusable and ignore all events.                                                             | `boolean \| undefined`                                                               | `false`     |
| `_label` _(required)_ | `_label`    | Defines the visible or semantic label of the component (e.g. aria-label, label, headline, caption, summary, etc.). | `string`                                                                             | `undefined` |
| `_level`              | `_level`    | Defines which H-level from 1-6 the heading has. 0 specifies no heading and is shown as bold text.                  | `0 \| 1 \| 2 \| 3 \| 4 \| 5 \| 6 \| undefined`                                       | `3`         |
| `_on`                 | --          | Gibt die EventCallback-Funktionen an.                                                                              | `undefined \| { onClick?: EventValueOrEventCallback<Event, boolean> \| undefined; }` | `undefined` |
| `_open`               | `_open`     | If set (to true) opens/expands the element, closes if not set (or set to false).                                   | `boolean \| undefined`                                                               | `false`     |

---
