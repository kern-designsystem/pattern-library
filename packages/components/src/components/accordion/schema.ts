import { Generic } from 'adopted-style-sheets';
import { HeadingLevel, KoliBriAccordionCallbacks } from '@public-ui/schema';

/**
 * API
 */
export type RequiredProps = {
	label: string;
};

export type OptionalProps = {
	disabled: boolean;
	level: HeadingLevel;
	on: KoliBriAccordionCallbacks;
	open: boolean;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = RequiredProps;

export type OptionalStates = {
	disabled: boolean;
	level: HeadingLevel;
	on: KoliBriAccordionCallbacks;
	open: boolean;
};

export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
