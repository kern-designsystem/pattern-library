import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, Prop, State, Watch } from '@stencil/core';
import { OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import { IdPropType, KoliBriModalEventCallbacks, LabelPropType } from '@public-ui/schema';

@Component({
	tag: 'kern-modal',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernModal implements Generic.Element.ComponentApi<RequiredProps, NonNullable<unknown>, RequiredStates, OptionalStates> {
	/**
	 * Gibt die Referenz auf das auslösende HTML-Element an, wodurch das Modal geöffnet wurde.
	 */
	@Prop({ mutable: true }) public _activeElement?: HTMLElement | null;

	/**
	 * Defines the visible or semantic label of the component (e.g. aria-label, label, headline, caption, summary, etc.).
	 */
	@Prop() public _label!: LabelPropType;

	/**
	 * Gibt die EventCallback-Function für das Schließen des Modals an.
	 */
	@Prop() public _on?: KoliBriModalEventCallbacks;

	/**
	 * Defines the width of the modal. (max-width: 100%)
	 */
	@Prop() public _width?: string = '100%';

	/**
	 * Defines the internal ID of the primary component element.
	 */
	@Prop() public _id?: IdPropType;

	@State() public state: States = {
		_label: '',
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		// watchString(this, '_label', value);
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}
	public componentWillLoad(): void {
		this.validateLabel(this._label);
	}

	public render(): JSX.Element {
		return (
			<kol-modal id={this._id} _label={this.state._label} _on={this._on} _width={this._width} _activeElement={this._activeElement}>
				<slot />
			</kol-modal>
		);
	}
}
