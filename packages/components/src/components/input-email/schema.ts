import { Generic } from 'adopted-style-sheets';
import {
	ButtonProps,
	IdPropType,
	InputTypeOnDefault,
	InputTypeOnOff,
	KoliBriIconsProp,
	LabelWithExpertSlotPropType,
	NamePropType,
	Stringified,
	SuggestionsPropType,
	SyncValueBySelectorPropType,
	TooltipAlignPropType,
} from '@public-ui/schema';
import { KernIconProp } from '../button/schema';

/**
 * API
 */
export type RequiredProps = {
	label: string;
	placeholder: string;
};

export type OptionalProps = {
	icons: KernIconProp;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	label: LabelWithExpertSlotPropType;
	placeholder: string;
};

export type OptionalStates = {
	accessKey: string;
	alert: boolean;
	autoComplete: InputTypeOnOff;
	disabled: boolean;
	error: string;
	hasCounter: boolean;
	hideError: boolean;
	hideLabel: boolean;
	hint: string;
	id: IdPropType;
	maxLength: number;
	name: NamePropType;
	on: InputTypeOnDefault;
	pattern: string;
	readOnly: boolean;
	icons: Stringified<KoliBriIconsProp>;
	suggestions: SuggestionsPropType;
	smartButton: Stringified<ButtonProps>;
	syncValueBySelector: SyncValueBySelectorPropType;
	tabIndex: number;
	tooltipAlign: TooltipAlignPropType;
	touched: boolean;
	value: string;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
