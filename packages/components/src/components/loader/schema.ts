import { Generic } from 'adopted-style-sheets';
import { ShowPropType, SpinVariantPropType } from '@public-ui/schema';

export type RequiredProps = NonNullable<unknown>;

export type OptionalProps = {
	show: ShowPropType;
	variant: SpinVariantPropType;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = RequiredProps;

export type OptionalStates = OptionalProps;
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
