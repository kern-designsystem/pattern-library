import { Generic } from 'adopted-style-sheets';
import { HasCloserPropType, IdPropType, LabelPropType } from '@public-ui/schema';

/**
 * API
 */
export type RequiredProps = {
	label: LabelPropType;
};

export type OptionalProps = {
	id: IdPropType;
	customClass: string;
	hasCloser: HasCloserPropType;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = RequiredProps;

export type OptionalStates = OptionalProps;

export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
