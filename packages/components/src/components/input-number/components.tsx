import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, State, Prop, Watch } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States, KernIconProp } from './schema';
import {
	ButtonProps,
	IdPropType,
	InputTypeOnDefault,
	InputTypeOnOff,
	Iso8601,
	LabelWithExpertSlotPropType,
	MsgPropType,
	NamePropType,
	Stringified,
	SuggestionsPropType,
	TooltipAlignPropType,
} from '@public-ui/schema';
@Component({
	tag: 'kern-input-number',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernInputNumber implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	// Props

	/**
	 * Definiert, welche Tastenkombination verwendet werden kann, um das interaktive Element des Komponenten auszulösen oder zu fokussieren.
	 */
	@Prop() public _accessKey?: string;

	/**
	 * Legt fest, ob das Eingabefeld automatisch vervollständigt werden kann.
	 */
	@Prop() public _autoComplete?: InputTypeOnOff;

	/**
	 * Gibt den Text des Inputs an.
	 */
	@Prop() public _label!: LabelWithExpertSlotPropType;

	/**
	 * Definiert den Hinweistext.
	 */
	@Prop() public _hint?: string = '';

	/**
	 * Gibt den Namen des Inputs an.
	 */
	@Prop() public _name?: NamePropType;

	/**
	 * Gibt die Fehlermeldung des Inputs an.
	 */
	@Prop() public _error?: string;

	/**
	 * Gibt die ID des Inputs an.
	 */
	@Prop() public _id!: IdPropType;

	/**
	 * Gibt den Placeholder des Inputs an.
	 */
	@Prop() public _placeholder?: string;

	/**
	 * Deaktiviert den Input.
	 * @TODO: Change type back to `DisabledPropType` after Stencil#4663 has been resolved.
	 */
	@Prop() public _disabled?: boolean = false;

	/**
	 * Versteckt die Fehlermeldung.
	 * @TODO: Change type back to `HideErrorPropType` after Stencil#4663 has been resolved.
	 */
	@Prop({ mutable: true, reflect: true }) public _hideError?: boolean = false;

	/**
	 * Blendet die Beschriftung (Label) aus und zeigt sie stattdessen mittels eines Tooltips an.
	 * @TODO: Change type back to `HideLabelPropType` after Stencil#4663 has been resolved.
	 */
	@Prop() public _hideLabel?: boolean = false;

	/**
	 * Defines the largest possible input value.
	 */
	@Prop() public _max?: number | Iso8601;

	/**
	 * Defines the smallest possible input value.
	 */
	@Prop() public _min?: number | Iso8601;

	/**
	 * Definiert die Klassennamen des Symbols (z.B. `_icons="material-symbols filled rounded search"`).
	 */
	@Prop() public _icons?: KernIconProp;

	/**
	 * Defines the step size for value changes.
	 */
	@Prop() public _step?: number;

	/**
	 * Gibt die EventCallback-Funktionen für das Input-Event an.
	 */
	@Prop() public _on?: InputTypeOnDefault;

	/**
	 * Suggestions to provide for the input.
	 */
	@Prop() public _suggestions?: SuggestionsPropType;

	/**
	 * Macht das Eingabeelement schreibgeschützt.
	 */
	@Prop() public _readOnly?: boolean = false;

	/**
	 * Macht das Eingabeelement erforderlich.
	 */
	@Prop() public _required?: boolean = false;

	@Prop() public _tooltipAlign?: TooltipAlignPropType = 'top';

	/**
	 * Zeigt an, ob die Eingabe von einem Benutzer berührt wurde.
	 */
	@Prop({ mutable: true, reflect: true }) public _touched?: boolean = false;

	/**
	 * Gibt den Value des Inputs an.
	 */
	@Prop({ mutable: true }) public _value?: number | Iso8601 | null;

	/**
	 * Defines which tab-index the primary element of the component has. (https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex)
	 */
	@Prop() public _tabIndex?: number;

	/**
	 * Allows to add a button with an arbitrary action within the element (_hide-label only).
	 */
	@Prop() public _smartButton?: Stringified<ButtonProps>;

	/**
	 * Defines the properties for a message rendered as Alert component.
	 */
	@Prop() public _msg?: MsgPropType;

	// States
	@State() public state: States = {
		_label: '',
		_id: '',
		_icons: {},
		_hideLabel: false,
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}

	@Watch('_icons')
	public validateIcons(value?: KernIconProp): void {
		if (typeof value === 'string') {
			this.state._icons = value;
		}
	}

	public componentWillLoad(): void {
		this.validateLabel(this._label);
	}

	public render(): JSX.Element {
		return (
			<kol-input-number
				class={{
					'hide-label': this._hideLabel === true,
				}}
				_id={this._id}
				_label={this.state._label}
				_value={this._value}
				_hint={this._hint}
				_on={this._on}
				_suggestions={this._suggestions}
				_hideLabel={this._hideLabel}
				_disabled={this._disabled}
				_error={this._error}
				_name={this._name}
				_min={this._min}
				_max={this._max}
				_step={this._step}
				_icons={this.state._icons}
				_autoComplete={this._autoComplete}
				_accessKey={this._accessKey}
				_readOnly={this._readOnly}
				_required={this._required}
				_tooltipAlign={this._tooltipAlign}
				_placeholder={this._placeholder}
				_tabIndex={this._tabIndex}
				_smartButton={this._smartButton}
				_msg={this._msg}
			></kol-input-number>
		);
	}
}
