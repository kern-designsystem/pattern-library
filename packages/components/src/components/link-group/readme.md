# kern-link-group

<!-- Auto Generated Below -->

## Properties

| Property              | Attribute          | Description                                                                                                        | Type                                        | Default      |
| --------------------- | ------------------ | ------------------------------------------------------------------------------------------------------------------ | ------------------------------------------- | ------------ |
| `_label` _(required)_ | `_label`           | Defines the visible or semantic label of the component (e.g. aria-label, label, headline, caption, summary, etc.). | `string`                                    | `undefined`  |
| `_links` _(required)_ | --                 | Defines the list of links to render.                                                                               | `LinkProps[]`                               | `undefined`  |
| `_listStyleType`      | `_list-style-type` | Gibt den List-Style-Typen für ungeordnete Listen aus. Wird bei horizontalen LinkGroups als Trenner verwendet       | `"circle" \| "disc" \| "none" \| undefined` | `'disc'`     |
| `_orientation`        | `_orientation`     | Defines whether the orientation of the component is horizontal or vertical.                                        | `"horizontal" \| "vertical" \| undefined`   | `'vertical'` |

---
