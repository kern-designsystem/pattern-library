import { Generic } from 'adopted-style-sheets';
import {
	IdPropType,
	InputCheckboxIconsProp,
	InputCheckboxVariant,
	InputTypeOnDefault,
	LabelWithExpertSlotPropType,
	NamePropType,
	StencilUnknown,
	Stringified,
	SyncValueBySelectorPropType,
	TooltipAlignPropType,
} from '@public-ui/schema';
import { KernIconProp } from '../icon/schema';

export type RequiredProps = {
	label: string;
};

export type OptionalProps = {
	icons: KernIconProp;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	label: LabelWithExpertSlotPropType;
};

export type OptionalStates = {
	id: IdPropType;
	alert: boolean;
	checked: boolean;
	hideError: boolean;
	hint: string;
	disabled: boolean;
	hideLabel: boolean;
	value: Stringified<StencilUnknown>;
	variant: InputCheckboxVariant;
	touched: boolean;
	required: boolean;
	icons: Stringified<InputCheckboxIconsProp>;
	indeterminate: boolean;
	name: NamePropType;
	on: InputTypeOnDefault;
	syncValueBySelector: SyncValueBySelectorPropType;
	tabIndex: number;
	tooltipAlign: TooltipAlignPropType;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
