import { KernAccordion } from './accordion/components';
import { KernAccordionGroup } from './accordion-group/components';
import { KernButton } from './button/components';
import { KernHeading } from './heading/components';
import { KernBadge } from './badge/components';
import { KernAlert } from './alert/components';
import { KernLinkGroup } from './link-group/components';
import { KernButtonGroup } from './button-group/components';
import { KernLoader } from './loader/components';
import { KernDivider } from './divider/components';
import { KernInputText } from './input-text/components';
import { KernInputEmail } from './input-email/components';
import { KernInputNumber } from './input-number/components';
import { KernIcon } from './icon/components';
import { KernInputPassword } from './input-password/components';
import { KernInputCheckbox } from './input-checkbox/components';
import { KernInputDate } from './input-date/components';
import { KernInputFile } from './input-file/components';
import { KernProgress } from './progress/components';
import { KernSelect } from './select/components';
import { KernForm } from './form/components';
import { KernText } from './text/components';
import { KernTextarea } from './textarea/components';
import { KernModal } from './modal/components';
import { KernFieldset } from './fieldset/components';
import { KernLinkButton } from './link-button/components';
import { KernKopfzeile } from './kopfzeile/components';
import { KernDialog } from './dialog/components';

export const COMPONENTS = [
	KernAccordion,
	KernAccordionGroup,
	KernButton,
	KernHeading,
	KernBadge,
	KernAlert,
	KernLoader,
	KernLinkGroup,
	KernDivider,
	KernDialog,
	KernInputText,
	KernIcon,
	KernLinkButton,
	KernInputPassword,
	KernInputCheckbox,
	KernInputFile,
	KernInputDate,
	KernProgress,
	KernSelect,
	KernInputEmail,
	KernTextarea,
	KernText,
	KernInputNumber,
	KernForm,
	KernButtonGroup,
	KernModal,
	KernFieldset,
	KernKopfzeile,
];
