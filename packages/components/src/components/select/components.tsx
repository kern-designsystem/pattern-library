import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, State, Prop, Watch } from '@stencil/core';
import { OptionalProps, OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import { KernIconProp } from '../button/schema';
import {
	IdPropType,
	InputTypeOnDefault,
	InputTypeOnOff,
	LabelWithExpertSlotPropType,
	NamePropType,
	OptionsWithOptgroupPropType,
	RowsPropType,
	Stringified,
	SyncValueBySelectorPropType,
	TooltipAlignPropType,
	W3CInputValue,
} from '@public-ui/schema';

@Component({
	tag: 'kern-select',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernSelect implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Definiert, welche Tastenkombination verwendet werden kann, um das interaktive Element der Komponenten auszulösen oder zu fokussieren.
	 */
	@Prop() public _accessKey?: string;

	/**
	 * Definiert, ob die Bildschirmleser die Benachrichtigung vorlesen sollen.
	 */
	@Prop({ mutable: true, reflect: true }) public _alert?: boolean = true;

	/**
	 * Legt fest, ob das Eingabefeld automatisch vervollständigt werden kann.
	 */
	@Prop() public _autoComplete?: InputTypeOnOff;

	/**
	 * Macht das Element nicht fokussierbar und ignoriert alle Ereignisse.
	 */
	@Prop() public _disabled?: boolean = false;

	/**
	 * Definiert den Text der Fehlermeldung.
	 */
	@Prop() public _error?: string;

	/**
	 * Versteckt die Fehlermeldung, lässt sie aber im DOM für das aria-describedby der Eingabe.
	 */
	@Prop({ mutable: true, reflect: true }) public _hideError?: boolean = false;

	/**
	 * Versteckt die Beschriftung standardmäßig und zeigt den Beschriftungstext mit einem Tooltip an, wenn das interaktive Element fokussiert ist oder die Maus darüber ist.
	 */
	@Prop() public _hideLabel?: boolean = false;

	/**
	 * Definiert den Hinweistext.
	 */
	@Prop() public _hint?: string = '';

	/**
	 * Definiert die Klassennamen des Symbols (z.B. `_icons="material-symbols filled rounded search"`).
	 */
	@Prop() public _icons?: KernIconProp;

	/**
	 * Definiert die interne ID des primären Komponentenelements.
	 */
	@Prop() public _id?: IdPropType;

	/**
	 * Definiert das sichtbare oder semantische Label der Komponente (z.B. aria-label, label, Überschrift, Beschriftung, Zusammenfassung, etc.). Setzen Sie auf `false`, um den Experten-Slot zu aktivieren.
	 */
	@Prop() public _label!: LabelWithExpertSlotPropType;

	/**
	 * Lässt das Eingabefeld mehrere Eingaben akzeptieren.
	 */
	@Prop() public _multiple?: boolean = false;

	/**
	 * Definiert den technischen Namen eines Eingabefelds.
	 */
	@Prop() public _name?: NamePropType;

	/**
	 * Gibt die EventCallback-Funktionen für das Input-Event an.
	 */
	@Prop() public _on?: InputTypeOnDefault;

	/**
	 * Optionen, aus denen der Benutzer auswählen kann, unterstützt auch Optgroup.
	 */
	@Prop() public _options?: OptionsWithOptgroupPropType;

	/**
	 * Definiert, wie viele Reihen von Optionen gleichzeitig sichtbar sein sollen.
	 */
	@Prop() public _rows?: RowsPropType;

	/**
	 * Macht das Eingabeelement schreibgeschützt.
	 */
	@Prop() public _readOnly?: boolean = false;

	/**
	 * Macht das Eingabeelement erforderlich.
	 */
	@Prop() public _required?: boolean = false;

	/**
	 * Selektor zur Synchronisierung des Wertes mit einem anderen Eingabeelement.
	 */
	@Prop() public _syncValueBySelector?: SyncValueBySelectorPropType;

	/**
	 * Definiert, welchen Tab-Index das primäre Element der Komponente hat. (https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex)
	 */
	@Prop() public _tabIndex?: number;

	/**
	 * Definiert, wo der Tooltip vorzugsweise angezeigt werden soll: oben, rechts, unten oder links.
	 */
	@Prop() public _tooltipAlign?: TooltipAlignPropType = 'top';

	/**
	 * Zeigt an, ob die Eingabe von einem Benutzer berührt wurde.
	 */
	@Prop({ mutable: true, reflect: true }) public _touched?: boolean = false;

	/**
	 * Definiert den Wert der Eingabe.
	 */
	@Prop({ mutable: true }) public _value?: Stringified<W3CInputValue[]>;

	// States
	@State() public state: States = {
		_hideError: false,
		_id: '',
		_label: '', // ⚠ required
		_multiple: false,
		_options: [],
		_value: [],
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}

	@Watch('_icons')
	public validateIcons(value?: KernIconProp): void {
		if (typeof value === 'string') {
			this.state._icons = value;
		}
	}

	public componentWillLoad(): void {
		this.validateLabel(this._label);
		this.validateIcons(this._icons);
	}

	public render(): JSX.Element {
		return (
			<kol-select
				class={{
					'hide-label': !!this._hideLabel,
					select: true,
				}}
				_accessKey={this._accessKey}
				_disabled={this._disabled}
				_error={this._error}
				_hideError={this._hideError}
				_hideLabel={this._hideLabel}
				_hint={this._hint}
				_icons={this.state._icons}
				_id={this._id}
				_on={this._on}
				_label={this.state._label}
				_readOnly={this._readOnly}
				_required={this._required}
				_tooltipAlign={this._tooltipAlign}
				_touched={this._touched}
				_value={this._value}
				_syncValueBySelector={this._syncValueBySelector}
				_tabIndex={this._tabIndex}
				_alert={this._alert}
				_multiple={this._multiple}
				_name={this._name}
				_options={this._options}
				_rows={this._rows}
			></kol-select>
		);
	}
}
