import { Generic } from 'adopted-style-sheets';
import {
	ButtonProps,
	IdPropType,
	InputDateType,
	InputTypeOnDefault,
	InputTypeOnOff,
	Iso8601,
	KoliBriIconsProp,
	LabelWithExpertSlotPropType,
	Stringified,
	SuggestionsPropType,
	TooltipAlignPropType,
} from '@public-ui/schema';

import { KernIconProp } from '../icon/schema';

export type RequiredProps = NonNullable<unknown>;

export type OptionalProps = {
	icons: KernIconProp;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	label: LabelWithExpertSlotPropType;
};

export type OptionalStates = {
	disabled: boolean;
	accessKey: string;
	autoComplete: InputTypeOnOff;
	error: string;
	hideError: boolean;
	hideLabel: boolean;
	hint: string;
	currentLength: number;
	id: IdPropType;
	max: Iso8601 | Date;
	min: Iso8601 | Date;
	name: string;
	on: InputTypeOnDefault;
	icons: Stringified<KoliBriIconsProp>;
	readOnly: boolean;
	required: boolean;
	suggestions: SuggestionsPropType;
	step: number;
	tabIndex: number;
	tooltipAlign: TooltipAlignPropType;
	touched: boolean;
	value: Iso8601 | Date | null;
	type: InputDateType;
	smartButton: Stringified<ButtonProps>;
};
export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
