import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, State, Prop, Watch } from '@stencil/core';
import { AllowedFileTypes, OptionalProps, OptionalStates, RequiredProps, RequiredStates, States, maxFileSize } from './schema';
import { KernIconProp } from '../icon/schema';
import {
	ButtonProps,
	IdPropType,
	InputTypeOnDefault,
	LabelWithExpertSlotPropType,
	NamePropType,
	Stringified,
	SyncValueBySelectorPropType,
	TooltipAlignPropType,
} from '@public-ui/schema';

@Component({
	tag: 'kern-input-file',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernInputFile implements Generic.Element.ComponentApi<RequiredProps, OptionalProps, RequiredStates, OptionalStates> {
	/**
	 * Defines which file formats are accepted.
	 */
	@Prop() public _accept?: string;

	/**
	 * Defines which key combination can be used to trigger or focus the interactive element of the component.
	 */
	@Prop() public _accessKey?: string;

	/**
	 * Defines whether the screen-readers should read out the notification.
	 */
	@Prop({ mutable: true, reflect: true }) public _alert?: boolean = true;

	/**
	 * Makes the element not focusable and ignore all events.
	 * @TODO: Change type back to `DisabledPropType` after Stencil#4663 has been resolved.
	 */
	@Prop() public _disabled?: boolean = false;

	/**
	 * Defines the error message text.
	 */
	@Prop() public _error?: string;

	/**
	 * Hides the error message but leaves it in the DOM for the input's aria-describedby.
	 * @TODO: Change type back to `HideErrorPropType` after Stencil#4663 has been resolved.
	 */
	@Prop({ mutable: true, reflect: true }) public _hideError?: boolean = false;

	/**
	 * Hides the caption by default and displays the caption text with a tooltip when the
	 * interactive element is focused or the mouse is over it.
	 * @TODO: Change type back to `HideLabelPropType` after Stencil#4663 has been resolved.
	 */
	@Prop() public _hideLabel?: boolean = false;

	/**
	 * Defines the hint text.
	 */
	@Prop() public _hint?: string = '';

	/**
	 * Defines the icon classnames (e.g. `_icons="fa-solid fa-user"`).
	 */
	@Prop() public _icons?: KernIconProp;

	/**
	 * Defines the internal ID of the primary component element.
	 */
	@Prop() public _id?: IdPropType;

	/**
	 * Defines the visible or semantic label of the component (e.g. aria-label, label, headline, caption, summary, etc.). Set to `false` to enable the expert slot.
	 */
	@Prop() public _label!: LabelWithExpertSlotPropType;

	/**
	 * Makes the input accept multiple inputs.
	 * @TODO: Change type back to `MultiplePropType` after Stencil#4663 has been resolved.
	 */
	@Prop() public _multiple?: boolean = false;

	/**
	 * Defines the technical name of an input field.
	 */
	@Prop() public _name?: NamePropType;

	/**
	 * Gibt die EventCallback-Funktionen für das Input-Event an.
	 */
	@Prop() public _on?: InputTypeOnDefault;

	/**
	 * Makes the input element required.
	 * @TODO: Change type back to `RequiredPropType` after Stencil#4663 has been resolved.
	 */
	@Prop() public _required?: boolean = false;

	/**
	 * Allows to add a button with an arbitrary action within the element (_hide-label only).
	 */
	@Prop() public _smartButton?: Stringified<ButtonProps>;

	/**
	 * Selector for synchronizing the value with another input element.
	 * @internal
	 */
	@Prop() public _syncValueBySelector?: SyncValueBySelectorPropType;

	/**
	 * Defines which tab-index the primary element of the component has. (https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex)
	 */
	@Prop() public _tabIndex?: number;

	/**
	 * Defines where to show the Tooltip preferably: top, right, bottom or left.
	 */
	@Prop() public _tooltipAlign?: TooltipAlignPropType = 'top';

	/**
	 * Shows if the input was touched by a user.
	 * @TODO: Change type back to `TouchedPropType` after Stencil#4663 has been resolved.
	 */
	@Prop({ mutable: true, reflect: true }) public _touched?: boolean = false;

	/**
	 * Defines the value of the input.
	 */
	@Prop() public _value?: string;

	/**
	 * Defines the maximum file size in bytes.
	 */
	@Prop() public _maxFileSize?: maxFileSize;

	/**
	 * Defines which file formats are accepted.
	 */
	@Prop() public _allowedFileTypes?: AllowedFileTypes[];

	// States
	@State() public state: States = {
		_icons: {},
		_label: '',
		_hideLabel: false,
		_disabled: false,
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}

	@Watch('_maxFileSize')
	public validateMaxFileSize(value?: number): void {
		if (typeof value === 'number') {
			this.state._maxFileSize = value;
		}
	}

	@Watch('_disabled')
	public validateDisabled(value?: boolean): void {
		if (typeof value === 'boolean') {
			this.state._disabled = value;
		}
	}

	@Watch('_icons')
	public validateIcons(value?: KernIconProp): void {
		if (typeof value === 'string') {
			this.state._icons = value;
		}
	}

	@Watch('_allowedFileTypes')
	public validateAllowedFileTypes(value?: AllowedFileTypes[]): void {
		if (Array.isArray(value) && value.length > 0) {
			this.state._allowedFileTypes = value;
		}
	}

	public componentWillLoad(): void {
		this.validateLabel(this._label);
		this.validateIcons(this._icons);
		this.validateMaxFileSize(this._maxFileSize);
		this.validateAllowedFileTypes(this._allowedFileTypes);
		this.validateDisabled(this._disabled);
	}

	public render(): JSX.Element {
		return (
			<kol-input-file
				class={{
					file: true,
					'hide-label': !!this._hideLabel,
				}}
				_maxFileSize={this._maxFileSize}
				_allowedFileTypes={this._allowedFileTypes}
				_accessKey={this._accessKey}
				_disabled={this.state._disabled}
				_error={this._error}
				_hideError={this._hideError}
				_hideLabel={this.state._hideLabel}
				_hint={this._hint}
				_icons={this.state._icons}
				_id={this._id}
				_label={this.state._label}
				_required={this._required}
				_smartButton={this._smartButton}
				_tooltipAlign={this._tooltipAlign}
				_touched={this._touched}
				_name={this._name}
				_value={this._value}
			>
				<slot name="expert" slot="expert" />
			</kol-input-file>
		);
	}
}
