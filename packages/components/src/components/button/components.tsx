import {
	AccessKeyPropType,
	AlternativeButtonLinkRolePropType,
	ButtonCallbacksPropType,
	ButtonTypePropType,
	CustomClassPropType,
	LabelWithExpertSlotPropType,
	StencilUnknown,
	Stringified,
	SyncValueBySelectorPropType,
	TooltipAlignPropType,
} from '@public-ui/schema';
import { Component, h, JSX, Prop, State, Watch } from '@stencil/core';
import { Generic } from 'adopted-style-sheets';
import { KernButtonVariant, KernIconProp, OptionalStates, RequiredStates, States } from './schema';

@Component({
	tag: 'kern-button',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernButton
	implements
		Generic.Element.ComponentApi<
			NonNullable<unknown>,
			{
				variant: KernButtonVariant;
			},
			RequiredStates,
			OptionalStates
		>
{
	/**
	 * Definiert die ID des interaktiven Elements der Komponente.
	 */
	@Prop() public _id?: string;

	/**
	 * Definiert den Namen des interaktiven Elements der Komponente.
	 */
	@Prop() public _name?: string;
	/**
	 * Defines the elements access key.
	 */
	@Prop() public _accessKey?: AccessKeyPropType;

	/**
	 * Definiert, welche Elemente von dieser Komponente gesteuert werden. (https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-controls)
	 */
	@Prop() public _ariaControls?: string;

	/**
	 * Definiert, ob das interaktive Element der Komponente etwas erweitert hat. (https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-expanded)
	 */
	@Prop() public _ariaExpanded?: boolean;

	/**
	 * Definiert, ob das interaktive Element der Komponente ausgewählt ist (z.B. role=tab). (https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-selected)
	 */
	@Prop() public _ariaSelected?: boolean;

	/**
	 * Gibt an, welche Custom-Class übergeben werden soll, wenn _variant="custom" gesetzt ist.
	 */
	@Prop() public _customClass?: CustomClassPropType;
	/**
	 * Gibt den Text des Buttons an.
	 */
	@Prop() public _label!: LabelWithExpertSlotPropType;

	/**
	 * Gibt an, ob der Button deaktiviert ist.
	 */
	@Prop() public _disabled?: boolean = false;

	/**
	 * Gibt an, welche Ausprägung der Button hat.
	 */
	@Prop() public _variant?: KernButtonVariant = 'primary';

	/**
	 * Blendet die Beschriftung (Label) aus und zeigt sie stattdessen mittels eines Tooltips an.
	 */
	@Prop() public _hideLabel?: boolean = false;

	/**
	 * Definiert entweder den Typ der Komponente oder des interaktiven Elements der Komponente.
	 */
	@Prop() public _type?: ButtonTypePropType = 'button';

	/**
	 * Setzt die Iconklasse (z.B.: `_icon="material-symbols-outlined home"`).
	 */
	@Prop() public _icons?: KernIconProp;

	/**
	 * Definiert die Rückruffunktionen für Button-Ereignisse.
	 */
	@Prop() public _on?: ButtonCallbacksPropType<StencilUnknown>;

	/**
	 * Defines the role of the components primary element.
	 */
	@Prop() public _role?: AlternativeButtonLinkRolePropType;

	/**
	 * Selector for synchronizing the value with another input element.
	 * @internal
	 */
	@Prop() public _syncValueBySelector?: SyncValueBySelectorPropType;

	/**
	 * Defines which tab-index the primary element of the component has. (https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex)
	 */
	@Prop() public _tabIndex?: number;

	/**
	 * Defines where to show the Tooltip preferably: top, right, bottom or left.
	 */
	@Prop() public _tooltipAlign?: TooltipAlignPropType = 'top';

	/**
	 * Defines the value that the button emits on click.
	 */
	@Prop() public _value?: Stringified<StencilUnknown>;

	@State() public state: States = {};

	@Watch('_variant')
	public validateVariant(value?: KernButtonVariant): void {
		if (typeof value === 'string') {
			switch (value) {
				case 'primary':
					this.state._variant = 'primary';
					break;
				case 'secondary':
					this.state._variant = 'secondary';
					break;
				case 'tertiary':
					this.state._variant = 'ghost';
					break;
				case 'custom':
					this.state._variant = 'custom';
					break;
			}
		}
	}

	@Watch('_icons')
	public validateIcons(value?: KernIconProp): void {
		if (typeof value === 'string') {
			this.state._icons = value;
		}
	}

	public componentWillLoad(): void {
		this.validateVariant(this._variant);
		this.validateIcons(this._icons);
	}

	public render(): JSX.Element {
		return (
			<kol-button
				id={this._id}
				class={{
					button: true,
					[this.state._variant as string]: this.state._variant !== 'custom',
					[this._customClass as string]: this.state._variant === 'custom' && typeof this._customClass === 'string' && this._customClass.length > 0,
				}}
				_accessKey={this._accessKey}
				_ariaControls={this._ariaControls}
				_ariaExpanded={this._ariaExpanded}
				_ariaSelected={this._ariaSelected}
				_customClass={this._customClass}
				_disabled={this._disabled}
				_hideLabel={this._hideLabel}
				_icons={this.state._icons}
				_id={this._id}
				_label={this._label}
				_name={this._name}
				_on={this._on}
				_role={this._role}
				_syncValueBySelector={this._syncValueBySelector}
				_tabIndex={this._tabIndex}
				_tooltipAlign={this._tooltipAlign}
				_type={this._type}
				_value={this._value}
				_variant={this.state._variant}
			>
				<slot name="expert" slot="expert"></slot>
			</kol-button>
		);
	}
}
