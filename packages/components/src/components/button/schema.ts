import { Generic } from 'adopted-style-sheets';
import {
	AccessKeyPropType,
	AlternativeButtonLinkRolePropType,
	ButtonCallbacksPropType,
	ButtonTypePropType,
	ButtonVariantPropType,
	CustomClassPropType,
	IconsPropType,
	InputTypeOnOff,
	LabelWithExpertSlotPropType,
	StencilUnknown,
	Stringified,
	SyncValueBySelectorPropType,
	TooltipAlignPropType,
} from '@public-ui/schema';

export type KernIconProp = string;

/** Variant property **/
export type KernButtonVariant = 'primary' | 'secondary' | 'tertiary' | 'custom';

export type RequiredProps = {
	label: LabelWithExpertSlotPropType;
};

export type OptionalProps = {
	icons: KernIconProp;
	variant: KernButtonVariant;
	on: ButtonCallbacksPropType<StencilUnknown>;
	id: string;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = NonNullable<unknown>;

export type OptionalStates = {
	autoComplete: InputTypeOnOff;
	id: string;
	label: LabelWithExpertSlotPropType;
	name: string;
	accessKey: AccessKeyPropType;
	ariaControls: string;
	ariaExpanded: boolean;
	ariaSelected: boolean;
	disabled: boolean;
	hideError: boolean;
	type: ButtonTypePropType;
	icons: IconsPropType;
	variant: ButtonVariantPropType;
	customClass: CustomClassPropType;
	hideLabel: boolean;
	role: AlternativeButtonLinkRolePropType;
	syncValueBySelector: SyncValueBySelectorPropType;
	on: ButtonCallbacksPropType<StencilUnknown>;
	tabIndex: number;
	tooltipAlign: TooltipAlignPropType;
	value: Stringified<StencilUnknown>;
};

export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
