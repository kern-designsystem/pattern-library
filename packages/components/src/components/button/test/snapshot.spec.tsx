import { h } from '@stencil/core';
import { newSpecPage, SpecPage } from '@stencil/core/testing';

import { COMPONENTS } from '../../component-list';
import { executeTests } from 'stencil-awesome-test';
import { getButtonHtml } from './html.mock';
import { Props } from '../schema';

executeTests<Props>(
	'Button',
	async (props): Promise<SpecPage> => {
		return await newSpecPage({
			components: COMPONENTS,
			template: () => <kern-button {...props} />,
		});
	},
	{
		_label: ['Hello world!'],
		_variant: ['primary', 'secondary'],
	},
	getButtonHtml,
	{
		execMode: 'default',
	},
);
