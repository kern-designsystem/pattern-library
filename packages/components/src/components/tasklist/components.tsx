import { Generic } from 'adopted-style-sheets';
import { Component, h, JSX, Prop, State, Watch, Element } from '@stencil/core';
import { OptionalStates, RequiredProps, RequiredStates, States } from './schema';
import { IdPropType, LabelPropType } from '@public-ui/schema';

@Component({
	tag: 'kern-tasklist',
	shadow: false,
	styleUrls: {
		default: './style.css',
	},
})
export class KernTasklist implements Generic.Element.ComponentApi<RequiredProps, NonNullable<unknown>, RequiredStates, OptionalStates> {
	/**
	 * Defines the visible or semantic label of the component (e.g. aria-label, label, headline, caption, summary, etc.).
	 */
	@Prop() public _label!: LabelPropType;

	@Element() host!: HTMLDivElement;

	/**
	 * Defines the internal ID of the primary component element.
	 */
	@Prop() public _id?: IdPropType;

	@State() public state: States = {
		_label: '',
	};

	@Watch('_label')
	public validateLabel(value?: string): void {
		if (typeof value === 'string') {
			this.state._label = value;
		}
	}

	public componentWillLoad(): void {
		this.validateLabel(this._label);
		this.prependSequenceToChildren();
	}

	private prependSequenceToChildren(): void {
		const slottedElements = Array.from(this.host.querySelectorAll('kern-tasklist-item'));
		let indexCounter = 1; // Counter for main items
		slottedElements.forEach((element) => {
			// Add unique id for group items based on their indices
			element.setAttribute('_id', `${this._id}_tasklistitem_${indexCounter}`);
			indexCounter++;
		});
	}

	render(): JSX.Element {
		return (
			<div class="tasklist">
				<ol class="tasklist__list">
					<slot></slot>
				</ol>
			</div>
		);
	}
}
