import { Generic } from 'adopted-style-sheets';
import { HeadingLevel, AlertType, AlertVariant, KoliBriAlertEventCallbacks, LabelPropType } from '@public-ui/schema';

/** Icon type for any icon Class **/
export type KernIconProp = string;

/** Variant property **/
export type KernAlertType = 'primary' | 'secondary' | 'info' | 'success' | 'error' | 'warning' | 'form-error';
export type KernLevelVariant = 1 | 2 | 3 | 4 | 5 | 6;

export type KernAlertVariant = 'card' | 'msg';
/**
 * API
 */
export type RequiredProps = {
	label: string;
};

export type OptionalProps = {
	level: KernLevelVariant;
	type: KernAlertType;
	variant: KernAlertVariant;
};

export type Props = Generic.Element.Members<RequiredProps, OptionalProps>;

export type RequiredStates = {
	label: LabelPropType;
};
export type OptionalStates = {
	alert: boolean;
	hasCloser: boolean;
	level: HeadingLevel;
	type: AlertType;
	variant: AlertVariant;
	icon: string;
	customClass: string;
	on: KoliBriAlertEventCallbacks;
};

export type States = Generic.Element.Members<RequiredStates, OptionalStates>;
