# kern-alert

<!-- Auto Generated Below -->

## Properties

| Property              | Attribute       | Description                                                          | Type                                                                                                   | Default     |
| --------------------- | --------------- | -------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------ | ----------- |
| `_alert`              | `_alert`        | Defines whether the screen-readers should read out the notification. | `boolean \| undefined`                                                                                 | `false`     |
| `_customClass`        | `_custom-class` | Gibt die Möglichkeit, eine eigene CSS Klasse zu ergänzen.            | `string \| undefined`                                                                                  | `undefined` |
| `_hasCloser`          | `_has-closer`   | Defines whether the element can be closed.                           | `boolean \| undefined`                                                                                 | `false`     |
| `_label` _(required)_ | `_label`        | Gibt den Text des Alerts an.                                         | `string`                                                                                               | `undefined` |
| `_level`              | `_level`        | Gibt die Wertigkeit der Alert Überschrift an.                        | `1 \| 2 \| 3 \| 4 \| 5 \| 6 \| undefined`                                                              | `5`         |
| `_on`                 | --              | Gibt die EventCallback-Function für das Schließen des Alerts an.     | `undefined \| { onClose?: EventCallback<Event> \| undefined; }`                                        | `undefined` |
| `_type`               | `_type`         | Gibt den Typ des Alerts an.                                          | `"error" \| "form-error" \| "info" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `'success'` |
| `_variant`            | `_variant`      | Gibt die Variante des Alerts an.                                     | `"card" \| "msg" \| undefined`                                                                         | `'msg'`     |

---
