# kern-link

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute             | Description                                                                                                                                      | Type                                                                                   | Default     |
| --------------------- | --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------- | ----------- |
| `_accessKey`          | `_access-key`         | Defines the elements access key.                                                                                                                 | `string` \| `undefined`                                                                  | `undefined` |
| `_ariaCurrentValue`   | `_aria-current-value` | Defines the value for the aria-current attribute.                                                                                                | `"date"` \| `"false"` \| `"location"` \| `"page"` \| `"step"` \| `"time"` \| `"true"` \| `undefined` | `undefined` |
| `_download`           | `_download`           | Teilt dem Browser mit, dass sich hinter dem Link eine Datei befindet. Setzt optional den Dateinamen.                                             | `string` \| `undefined`                                                                  | `undefined` |
| `_hideLabel`          | `_hide-label`         | Blendet die Beschriftung (Label) aus und zeigt sie stattdessen mittels eines Tooltips an.                                                        | `boolean` \| `undefined`                                                                 | `false`     |
| `_href` _(required)_  | `_href`               | Gibt die Ziel-Url des Links an.                                                                                                                  | `string`                                                                               | `undefined` |
| `_icons`              | `_icons`              | Setzt die Iconklasse (z.B.: `_icon="material-symbols-outlined home"`).                                                                           | `string` \| `undefined`                                                                  | `undefined` |
| `_label` _(required)_ | `_label`              | Setzt die sichtbare oder semantische Beschriftung der Komponente (z.B. Aria-Label, Label, Headline, Caption, Summary usw.).                      | `string`                                                                               | `undefined` |
| `_on`                 | --                    | Defines the callback functions for links.                                                                                                        | `undefined` \| `{ onClick?: EventValueOrEventCallback<Event, string>` \| `undefined; }`    | `undefined` |
| `_role`               | `_role`               | Defines the role of the components primary element.                                                                                              | `"button"` \| `"link"` \| `"tab"` \| `undefined`                                             | `undefined` |
| `_tabIndex`           | `_tab-index`          | Defines which tab-index the primary element of the component has. (https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) | `number` \| `undefined`                                                                  | `undefined` |
| `_target`             | `_target`             | Defines where to open the link.                                                                                                                  | `string` \| `undefined`                                                                  | `undefined` |
| `_tooltipAlign`       | `_tooltip-align`      | Defines where to show the Tooltip preferably: top, right, bottom or left.                                                                        | `"bottom"` \| `"left"` \| `"right"` \| `"top"` \| `undefined`                                  | `'right'`   |


----------------------------------------------


