# kern-button

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute        | Description                                                                                                                                                                     | Type                                                                                                                                                   | Default     |
| --------------------- | ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------- |
| `_accessKey`          | `_access-key`    | Defines the elements access key.                                                                                                                                                | `string` \| `undefined`                                                                                                                                  | `undefined` |
| `_ariaControls`       | `_aria-controls` | Definiert, welche Elemente von dieser Komponente gesteuert werden. (https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-controls)                   | `string` \| `undefined`                                                                                                                                  | `undefined` |
| `_ariaExpanded`       | `_aria-expanded` | Definiert, ob das interaktive Element der Komponente etwas erweitert hat. (https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-expanded)            | `boolean` \| `undefined`                                                                                                                                 | `undefined` |
| `_ariaSelected`       | `_aria-selected` | Definiert, ob das interaktive Element der Komponente ausgewählt ist (z.B. role=tab). (https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-selected) | `boolean` \| `undefined`                                                                                                                                 | `undefined` |
| `_customClass`        | `_custom-class`  | Gibt an, welche Custom-Class übergeben werden soll, wenn _variant="custom" gesetzt ist.                                                                                         | `string` \| `undefined`                                                                                                                                  | `undefined` |
| `_disabled`           | `_disabled`      | Gibt an, ob der Button deaktiviert ist.                                                                                                                                         | `boolean` \| `undefined`                                                                                                                                 | `false`     |
| `_hideLabel`          | `_hide-label`    | Blendet die Beschriftung (Label) aus und zeigt sie stattdessen mittels eines Tooltips an.                                                                                       | `boolean` \| `undefined`                                                                                                                                 | `false`     |
| `_icons`              | `_icons`         | Setzt die Iconklasse (z.B.: `_icon="material-symbols-outlined home"`).                                                                                                          | `string` \| `undefined`                                                                                                                                  | `undefined` |
| `_id`                 | `_id`            | Definiert die ID des interaktiven Elements der Komponente.                                                                                                                      | `string` \| `undefined`                                                                                                                                  | `undefined` |
| `_label` _(required)_ | `_label`         | Gibt den Text des Buttons an.                                                                                                                                                   | `string`                                                                                                                                               | `undefined` |
| `_name`               | `_name`          | Definiert den Namen des interaktiven Elements der Komponente.                                                                                                                   | `string` \| `undefined`                                                                                                                                  | `undefined` |
| `_on`                 | --               | Definiert die Rückruffunktionen für Button-Ereignisse.                                                                                                                          | `undefined` \| `{ onClick?: EventValueOrEventCallback<MouseEvent, StencilUnknown>` \| `undefined; onMouseDown?: EventCallback<MouseEvent>` \| `undefined; }` | `undefined` |
| `_role`               | `_role`          | Defines the role of the components primary element.                                                                                                                             | `"button"` \| `"link"` \| `"tab"` \| `undefined`                                                                                                             | `undefined` |
| `_tabIndex`           | `_tab-index`     | Defines which tab-index the primary element of the component has. (https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex)                                | `number` \| `undefined`                                                                                                                                  | `undefined` |
| `_tooltipAlign`       | `_tooltip-align` | Defines where to show the Tooltip preferably: top, right, bottom or left.                                                                                                       | `"bottom"` \| `"left"` \| `"right"` \| `"top"` \| `undefined`                                                                                                  | `'top'`     |
| `_type`               | `_type`          | Definiert entweder den Typ der Komponente oder des interaktiven Elements der Komponente.                                                                                        | `"button"` \| `"reset"` \| `"submit"` \| `undefined`                                                                                                         | `'button'`  |
| `_value`              | `_value`         | Defines the value that the button emits on click.                                                                                                                               | `boolean` \| `null` \| `number` \| `object` \| `string` \| `undefined`                                                                                           | `undefined` |
| `_variant`            | `_variant`       | Gibt an, welche Ausprägung der Button hat.                                                                                                                                      | `"custom"` \| `"primary"` \| `"secondary"` \| `"tertiary"` \| `undefined`                                                                                      | `'primary'` |


## Dependencies


### Graph
```mermaid
graph TD;
  kern-dialog --> kern-button
  style kern-button stroke:#333,stroke-width:4px
```

----------------------------------------------


