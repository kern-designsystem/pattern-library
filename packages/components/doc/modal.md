# kern-modal

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute | Description                                                                                                        | Type                                                      | Default     |
| --------------------- | --------- | ------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------- | ----------- |
| `_activeElement`      | --        | Gibt die Referenz auf das auslösende HTML-Element an, wodurch das Modal geöffnet wurde.                            | `HTMLElement` \| `null` \| `undefined`                        | `undefined` |
| `_id`                 | `_id`     | Defines the internal ID of the primary component element.                                                          | `string` \| `undefined`                                     | `undefined` |
| `_label` _(required)_ | `_label`  | Defines the visible or semantic label of the component (e.g. aria-label, label, headline, caption, summary, etc.). | `string`                                                  | `undefined` |
| `_on`                 | --        | Gibt die EventCallback-Function für das Schließen des Modals an.                                                   | `undefined` \| `({ onClose?: (() => void)` \| `undefined; })` | `undefined` |
| `_width`              | `_width`  | Defines the width of the modal. (max-width: 100%)                                                                  | `string` \| `undefined`                                     | `'100%'`    |


----------------------------------------------


