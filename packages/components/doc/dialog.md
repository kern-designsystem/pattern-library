# kern-dialog

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute       | Description                                                                                                        | Type                   | Default     |
| --------------------- | --------------- | ------------------------------------------------------------------------------------------------------------------ | ---------------------- | ----------- |
| `_customClass`        | `_custom-class` | Gibt an, welche Custom-Class übergeben werden soll, wenn _variant="custom" gesetzt ist.                            | `string` \| `undefined`  | `undefined` |
| `_hasCloser`          | `_has-closer`   | Soll schließen Button angezeigt werden?                                                                            | `boolean` \| `undefined` | `undefined` |
| `_id`                 | `_id`           | Defines the internal ID of the primary component element.                                                          | `string` \| `undefined`  | `undefined` |
| `_label` _(required)_ | `_label`        | Defines the visible or semantic label of the component (e.g. aria-label, label, headline, caption, summary, etc.). | `string`               | `undefined` |


## Dependencies

### Depends on

- [kern-button](../button)
- [kern-heading](../heading)

### Graph
```mermaid
graph TD;
  kern-dialog --> kern-button
  kern-dialog --> kern-heading
  style kern-dialog stroke:#333,stroke-width:4px
```

----------------------------------------------


