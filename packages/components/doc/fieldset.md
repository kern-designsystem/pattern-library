# kern-text

<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description                                                                             | Type                                      | Default      |
| -------------- | --------------- | --------------------------------------------------------------------------------------- | ----------------------------------------- | ------------ |
| `_customClass` | `_custom-class` | Gibt an, welche Custom-Class übergeben werden soll, wenn _variant="custom" gesetzt ist. | `string` \| `undefined`                     | `undefined`  |
| `_legend`      | `_legend`       | Gibt an, welcher Text als Überschrift des Fieldsets angezeigt werden soll.              | `string` \| `undefined`                     | `undefined`  |
| `_orientation` | `_orientation`  | Defines whether the orientation of the component is horizontal or vertical.             | `"horizontal"` \| `"vertical"` \| `undefined` | `'vertical'` |


----------------------------------------------


