# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.5.14](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.13...v1.5.14) (2024-11-20)

### Bug Fixes

- add mobile styles for kopfzeile ([87204cb](https://gitlab.opencode.de/kern-ux/pattern-library/commit/87204cb799a97699674bca90f7bfbc877041da2e))

## [1.5.13](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.12...v1.5.13) (2024-11-15)

### Bug Fixes

- change alert border color to be more accessible ([9d8c781](https://gitlab.opencode.de/kern-ux/pattern-library/commit/9d8c7817f1b1816012aaf2c8dab462916c3c9aeb))

## [1.5.12](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.11...v1.5.12) (2024-11-08)

### Bug Fixes

- change font family for kopfzeile ([bd1a9c1](https://gitlab.opencode.de/kern-ux/pattern-library/commit/bd1a9c1424fc811e2f243ed26f617755b990aa81))

## [1.5.11](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.10...v1.5.11) (2024-10-14)

### Bug Fixes

- buttin with icon size ([#72](https://gitlab.opencode.de/kern-ux/pattern-library/issues/72)) ([dfcab91](https://gitlab.opencode.de/kern-ux/pattern-library/commit/dfcab918e8b736d039b0b2973fb4c019868e4589))

## [1.5.10](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.9...v1.5.10) (2024-10-14)

**Note:** Version bump only for package @kern-ux/monnorepro

## [1.5.9](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.8...v1.5.9) (2024-10-14)

**Note:** Version bump only for package @kern-ux/monnorepro

## [1.5.8](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.7...v1.5.8) (2024-10-14)

### Bug Fixes

- checkbox position on top of line ([#13](https://gitlab.opencode.de/kern-ux/pattern-library/issues/13)) ([1e55ab1](https://gitlab.opencode.de/kern-ux/pattern-library/commit/1e55ab1ef50eaef7c01e9e261d36a3beed9fbe5e))

## [1.5.7](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.6...v1.5.7) (2024-10-14)

### Bug Fixes

- space between task groups ([#5](https://gitlab.opencode.de/kern-ux/pattern-library/issues/5)) ([ad7759f](https://gitlab.opencode.de/kern-ux/pattern-library/commit/ad7759fcc788d7c5c810992342643025c3dfa960))

## [1.5.6](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.5...v1.5.6) (2024-10-11)

### Bug Fixes

- change style for radio fieldset label to float ([#11](https://gitlab.opencode.de/kern-ux/pattern-library/issues/11)) ([152c73e](https://gitlab.opencode.de/kern-ux/pattern-library/commit/152c73ec9015fb059784215cef2b5613ff4d69ce))
- padding bottom inside accordion contet ([#9](https://gitlab.opencode.de/kern-ux/pattern-library/issues/9)) ([878df08](https://gitlab.opencode.de/kern-ux/pattern-library/commit/878df084327c0b43aa8f7213bfcb1393b9d0d152))

## [1.5.5](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.4...v1.5.5) (2024-10-10)

### Bug Fixes

- kopfzeile border-box ([24872e2](https://gitlab.opencode.de/kern-ux/pattern-library/commit/24872e2e10e2fe9d5514d677166cc0bf760283ef))

## [1.5.4](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.3...v1.5.4) (2024-10-10)

### Bug Fixes

- font for kopfzeile ([4dbad74](https://gitlab.opencode.de/kern-ux/pattern-library/commit/4dbad74edb3a5cff339cd510a8babaa61d50202e))

## [1.5.3](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.2...v1.5.3) (2024-10-10)

### Bug Fixes

- change 0 to 15px for kopfzeilen padding ([0beb873](https://gitlab.opencode.de/kern-ux/pattern-library/commit/0beb873e80043c729e13bc2f84f23e60b1be0232))

## [1.5.2](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.1...v1.5.2) (2024-10-10)

### Bug Fixes

- remove 1400 breakpoint for kopfzeile ([5a7aaa4](https://gitlab.opencode.de/kern-ux/pattern-library/commit/5a7aaa479029c2fc02d48f2468a17e0eb7a8a464))

## [1.5.1](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.5.0...v1.5.1) (2024-10-10)

### Bug Fixes

- kopfzeile ([9eb4176](https://gitlab.opencode.de/kern-ux/pattern-library/commit/9eb4176e024cffa91895970171b8c604f66a0222))

# [1.5.0](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.4.0...v1.5.0) (2024-10-10)

### Features

- add \_fluid property to and custom font to kopfzeile ([#121](https://gitlab.opencode.de/kern-ux/pattern-library/issues/121)) ([c442502](https://gitlab.opencode.de/kern-ux/pattern-library/commit/c44250220d2956c386daca34a7e7b25fefd7f402))
- add ordered list markup and update style for tasklist items ([#63](https://gitlab.opencode.de/kern-ux/pattern-library/issues/63)) ([7fe94d5](https://gitlab.opencode.de/kern-ux/pattern-library/commit/7fe94d5573c3033d77fa78185892d01e23eee497))
- add weight property for bold text, to text component ([#64](https://gitlab.opencode.de/kern-ux/pattern-library/issues/64)) ([6004324](https://gitlab.opencode.de/kern-ux/pattern-library/commit/6004324415985cf239b489b616d4cf8d872b365d))

# [1.4.0](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.3.0...v1.4.0) (2024-10-02)

### Features

- add \_ariaHidden property to divider ([#70](https://gitlab.opencode.de/kern-ux/pattern-library/issues/70)) ([c08a1de](https://gitlab.opencode.de/kern-ux/pattern-library/commit/c08a1def1ee9713e1b807cd2c6153b5311791143))

# [1.3.0](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.2.21...v1.3.0) (2024-10-02)

### Bug Fixes

- Generation of ids in tasklist ([#68](https://gitlab.opencode.de/kern-ux/pattern-library/issues/68)) ([4707fd5](https://gitlab.opencode.de/kern-ux/pattern-library/commit/4707fd53babe588b740686841ab51a706a5acc7c))

### Features

- add visibility toggle to password ([#69](https://gitlab.opencode.de/kern-ux/pattern-library/issues/69)) ([c93a803](https://gitlab.opencode.de/kern-ux/pattern-library/commit/c93a803b141dafeaedd30ac01207dafdb4c9a3ae))

## [1.2.21](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.2.20...v1.2.21) (2024-09-26)

### Bug Fixes

- input-file property passthrough ([#67](https://gitlab.opencode.de/kern-ux/pattern-library/issues/67)) ([36fe752](https://gitlab.opencode.de/kern-ux/pattern-library/commit/36fe752fba35c9d1a7ae98081a59cff9dd077102))

## [1.2.20](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.2.19...v1.2.20) (2024-09-26)

### Bug Fixes

- change aria-describedby to aria-description ([#66](https://gitlab.opencode.de/kern-ux/pattern-library/issues/66)) ([fca1a27](https://gitlab.opencode.de/kern-ux/pattern-library/commit/fca1a27f1c0ffe43902d81ca87a54628f351f87f))

## [1.2.19](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.2.18...v1.2.19) (2024-09-26)

**Note:** Version bump only for package @kern-ux/monnorepro

## [1.2.18](https://gitlab.opencode.de/kern-ux/pattern-library/compare/v1.2.17...v1.2.18) (2024-09-26)

**Note:** Version bump only for package @kern-ux/monnorepro
